// vue basic lib
import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
Vue.use(Vuex)
Vue.use(VueCookies)

// 客製/第三方 lib
import md5 from 'md5'
import axios from 'axios'
// import { getDistance } from 'geolib'
import routerKakar from 'src/appKakar/router'
import routerWuhulife from 'src/appWuhulife/router'
import fetchData from '../libs/fetchData.js'
import { deviseFunction, ensureDeviseSynced, deviseSetLoginInfo, setRedirectPage } from 'src/libs/deviseHelper.js'
import { showAlert, setLoading } from 'src/libs/appHelper.js'
import { groupBy } from '../libs/market.js'

// 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
function rawRemberData() {
  return {
    isLoaded: false,
    // 會員卡
    card_info: {},
    // 會員資料
    vip_info: {},
    // (可使用的) 優惠卷清單
    ticketList: [],
    // (尚未能使用的) 優惠卷清單
    notAvailableTickets: [],
    // (已轉贈的) 優惠卷清單
    gaveTicketList: [],
    // 會員購買交易資料(有購買品項與總金額)
    purchaseRecord: [],
    // 會員積點交易資料(使用紀錄)
    expiredPoints: [],
    // (單一)優惠卷明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
    ticketInfo: {
      // 規則代號
      currentTypeCode: '',
      currentRuleCode: '',
      // 使用 QRcode
      currentTicketGID: '',
      currentTicketQRcode: '',
      // 規則條款
      ruleData: {},
      // 適用商品
      usageFoods: [],
      // 適用門店
      usageShops: [],
    }
  }
}

// 企業公開資料相關
function rawPublicData() {
  return {
    // LOGO 圖檔
    logo: '',
    logoLoaded: false,
    // DB connect 位置
    connect: '9001',
    // 首頁
    index: {
      slideBannerIsLoaded: false,
      subBannerIsLoaded: false,
      // 主畫面可滑動的 Banners
      slideBanners: [],
      // 固定的副 Banner
      subBanner: [],
    },
    // 品牌資訊(關於我們, 使用者條款, 常見問題 ... etc)
    brandInfo: [],
    // 最新消息
    news: { data: {}, types: {}, banners: {}, },
    // 菜單頁
    menu: {
      // 大類 (代號, 名稱, 清單)
      mainType: [], // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
      // 小類 (代號, 名稱, 清單)
      subType: [],  // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
    },
    // 門店資訊頁
    storeList: { store: [] },
    // 主企業號 ID & connect (用於線上點餐)
    MainEnterPriseID: '',
    MainConnect: '',
    myBookList: null, // 我的預約
    myBookStatusList: [] // 我的預約狀態 - S:已送出, P:處理中, Y:己確認, N:己取消
  }
}
// 商城共用資料相關
function rawMallData() {
  return {    
    foodkind2:[], //大類
    foodkind:[],//中類全部,未分
    kind:[],  //次類,內有foods(次類中的品項)    
    foodTaste:{}, //口味加料
    iv_foods:{},//小類下的所有品項
    iv_styles:{},//品項下的所有規格
    foodSpec:[],//規格
    foodSpecKind:[],//規格類別
    shipData:[],//收件人清單
    iv_spec:{},//品項規格
    orderstatus:{},//訂單狀態
    shopOrderTmp:{Total:0},//訂單主表
    orders:[], //訂單清單
    imgUrl: 'https://9001.jinher.com.tw/WebPos/images/Food/',
    nowItem:{},//當下所選品項
    spyIndex: 0,//規格index
    shopping_cart:[],//購物車
    foodQty:{},//品項計數
    kindQty:{},//品項類別計數
    isCarUI:false,
    Banner:[], //商城首頁橫幅
    Header:[], //推薦商品群組
    iv_currentItem:[], //所有商品
    foodInfo:{},//商品資料object
    foodFreight:{},//運費相關
    reloadShopFn:null,
    kind2Index:0, //記下目前進入的大類
    mallScrollTop:0,
    mallHomeScrollTop:0, //商城首頁,scrollTop位置
    sortPriceType:'',
    barcodeItem:[{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}],//由掃瞄生成的品項
    foodSpecLimit:[], //品項規則清單
    foodSpecLimitGp:{}, //品項規則
    foodSpecLimitData:{}, //品項規則(下拉數據源)
    checkKind_list:[],
    becomeApp:false,
    payStatus:{},//付款狀態
    ordersPage:{}, //訂單清單,分頁
    unOrdersPage:{}, //未付款訂單清單,分頁
  }
}
// 依據環境參數，來決定 API baseUrl 的值
const baseUrl = 'wuhuapp.jh8.tw'

const state = {
  appSite: 'kakar', // 系統參數: 站台
  promise: null, // 保存promise
  // 是否在殼裡面(判斷能否使用殼相關接口的依據)
  isDeviseApp: false,
  // 啟動/關閉 『讀取中』的動畫效果
  isLoading: false,
  marketBanner:{},//暫存卡包企業首頁進商城主橫幅
  // 顯示手動輸入 QRcode 表格
  showInputQRCode: false,
  // 客製的彈窗 (用於 alert / confirm)
  customModal: { 
    type: '', text: '', confirmText: '', cancelText: '',
    // confirm note
    confirmNoteTitle: '', // 附註標題
    confirmNoteChoice: [], // 附註選項
    confirmNoteChoosed: '', // 附註選項已選
    confirmNoteRemark: '', // 附註選了其他要手key的說明
    confirmNoteRemarkPlaceHolder: '', // 其他原因的place holder
    confirmNoteWarnMsg: '' // 警告訊息
  },
  // CSS 全域參數(用於判斷 CSS 是否設定 -webkit-overflow-scrolling: touch)
  // true => touch,
  // false => auto (幾乎只用於首頁)
  cssTouch: true,
  // API url
  api: {
    // 卡+ 主服務
    kakarUrl: `https://${baseUrl}/Public/AppNCWOneEntry.ashx`,
    // 企業的公開資料
    publicUrl: `https://${baseUrl}/public/newsocket.ashx`,
    // 企業的會員資料
    noTokenUrl: `https://${baseUrl}/Public/AppVip.ashx`,
    // 企業的會員資料
    memberUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,
    // 企業的會員消費點數資料
    consumerUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,
    // 預約資料
    bookUrl: `https://${baseUrl}/public/AppBooking.ashx`,
    // 預約的token資料
    tokenUrl: `https://${baseUrl}/public/AppBookingOneEntry.ashx`,
    // 圖片的路徑
    picUrl: `https://${baseUrl}`,
    // 會員資料存取,例:收件人存取
    vipUrl: `https://${baseUrl}/public/APPMemberOneEntry.ashx`,
  },
  // 基本資訊
  baseInfo: {
    // 英文常稱
    isFrom: 'kakar',
    // 企業號
    EnterpriseID: 'kakar',
    // FunctionID
    FunctionID: '450101',
    // 是否接收推播訊息 (跟殼要)
    pushNotify: '0',
    // GPS 資訊 (跟殼要)
    gps: {},
    // 螢幕亮度值 (跟殼要, range 0 ~ 255 預設為中間值)
    brightness: '120',
    // 前端打包檔版本號 (跟殼要)
    WebVer: '',
    // 殼的裝置 (iOS / Android)
    AppOS: '',
    // 殼是否有新版本
    DeviseVersionIsDiff: false,
    // (給線上點餐用) 回到 卡+ 時導向到指定企業 app
    currentEnterPriseID: (VueCookies.get('currentEnterPriseID') || ''),
    // 企業公開資料相關 (五互只需要rawPublicData()的這些欄位)
    qrcodeData:{},
    publicData: {
      // LOGO 圖檔
      logo: '',
      logoLoaded: false,
      // 首頁
      index: {
        slideBannerIsLoaded: false,
        subBannerIsLoaded: false,
        // 主畫面可滑動的 Banners
        slideBanners: [],
        // 固定的副 Banner
        subBanner: [],
      },
      AppName: '',
      // 最新消息
      news: { data: {}, types: {}, banners: {}, },
      // 門店資訊頁
      storeList: { store: [] },
      //storeFtype: { store: [] },
      storeFtype: [],
      myBookList: [] // 我的預約
    },
    // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
    memberData: {
      // 會員資料
      vip_info: {},
      // 會員卡
      card_info: {},
      // 積分換卷的資料
      pointToTickets: [],
      // 年度總積點資料
      expiredPoints_section: [],
      // 會員積點交易資料(使用紀錄)
      expiredPoints: [],
      // (可使用的) 優惠卷清單
      ticketList: [],
      // (尚未能使用的) 優惠卷清單
      notAvailableTickets: [],
      // (已轉贈的) 優惠卷清單
      gaveTicketList: [],
      // (單一)優惠卷明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
      ticketInfo: {
        // 規則代號
        currentTypeCode: '',
        currentRuleCode: '',
        // 使用 QRcode
        currentTicketGID: '',
        currentTicketQRcode: '',
        // 規則條款
        ruleData: {},
        // 適用商品
        usageFoods: [],
        // 適用門店
        usageShops: [],
      },
      consumerPoints: [], // 全部消費點數契約清單
      consumerPointDetail: {}, // 單一筆契約消費點數紀錄
      oneOrderDetail: [], // 單一筆訂單購物清單
      specShopList:[], //特約商清單,
      subscriptionList:[], //訂閱制清單
      isVipDataLoad:false, //是否已經撈setMemberVipDataWuhu
    }
  },
  // 簡訊認證設定 (需要從殼取)
  SMS_Config: {
    // 簡訊回傳的驗證碼
    reCode: "",
    // 每天最多簡訊次數 (預設為 5 次)
    APPSMSDayTimes: '5',
    // 每封簡訊發送間隔 (預設為 50 秒)
    AppSMSIntervalMin: '50',
    // 每日簡訊發送扣打 (用來紀錄已寄出幾封, 不得超過 APPSMSDayTimes 的值), 格式： yyyymmdd-times => e.g. 20190513-4 (05/13 已用四次)
    SMSSendQuota: '',
  },
  // 會員接口相關資訊
  member: {
    mac: (typeof(JSInterface) !== 'undefined')?'':(VueCookies.get('kkMAC') || ''),
    Tokenkey: (typeof(JSInterface) !== 'undefined')?'':(VueCookies.get('kkTokenkey') || ''),
    code: (typeof(JSInterface) !== 'undefined')?'':(VueCookies.get('kkUserCode') || ''),
    pwd: (typeof(JSInterface) !== 'undefined')?'':(VueCookies.get('kkUserPwd')||''),
  },
  // 首頁上面的卡面搜尋 bar
  appFilter: '',
  // 企業 app 的點擊次數統計
  appClicks: (VueCookies.get('kkAppClicks') ? VueCookies.get('kkAppClicks') : {}),
  vipAppsLoaded: false,
  // 已綁定的企業 app 列表
  vipApps: [],
  // 已綁定的企業 user 資料
  vipUserData: [],
  // 目前的進入/操作的 app
  currentAppUser: (typeof(JSInterface) !== 'undefined')?{}:(VueCookies.get('kkAppUser') ? VueCookies.get('kkAppUser') : {}),
  // (卡卡)會員帳號資訊
  userData: (typeof(JSInterface) !== 'undefined')?{}:(VueCookies.get('kkUserData') ? VueCookies.get('kkUserData') : {}),
  // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
  memberData: rawRemberData(),
  // 企業公開資料相關
  publicData: rawPublicData(),
   // 商城公開資料相關
  mallData: rawMallData(),

  // 卡片條碼 QRcode
  cardQRCode: { image: '', value: '', loaded: false },
  cardListScroll2: 0,
  fontSizeBase: 1,
  cardListScrollMap: 0, //最後一次scroll位置記錄

}

const mutations = {
  /** 暫存字體大小 */
  setFontSizeTemp(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
  },
  /** 永久保存字體大小 */
  setFontSizeForever(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
    // 存入cookie
    VueCookies.set('fontSize', floatFontSize)
    // 若在殼中 => 存入殼裡
    if (typeof(JSInterface) !== 'undefined') {
      let setThing = `{"spName":"fontSize", "spValue": ${ floatFontSize }}`
      deviseFunction('SetSP', setThing, '')
      // console.log('===> 已存入殼裡 =>' + setThing)
    }
  },
  // 存入系統參數：站台 & 基礎參數
  setAppSite(state, site) {
    state.appSite = site
    // 如果 site === 'wuhulife' , 要修改對應的 baseInfo.isFrom, baseInfo.EnterpriseID
    if (site === 'wuhulife' ) state.baseInfo.isFrom = 'wuhulifeapp'
    if (site === 'wuhulife' ) state.baseInfo.FunctionID = '450301'
    if (site === 'wuhulife' ) state.baseInfo.EnterpriseID = '53239538'
    if (site === 'wuhulife' ) state.baseInfo.AppName = '龍海就是消費'
  },
  //存入讀取的QRcode結果
  setQrcodeData(state, data) { state.qrcodeData = data },
  // 存入當下進入的企業 app 資料
  setCurrentAppUser(state, app) { VueCookies.set('kkAppUser', app); state.currentAppUser = app },
  // 清空當下進入的企業 app 資料
  clearCurrentAppUser(state) { VueCookies.remove('kkAppUser'); state.currentAppUser = {} },
  // 存入企業 app 列表的點擊紀錄 (整個 object)
  saveAppClicks(state, clicks) { state.appClicks = clicks },
  // 存入企業 app 列表的點擊紀錄 ( EnterpriseID += 1 )
  setAppClicks(state, eID) {
    state.appClicks[eID] = state.appClicks[eID] || 0
    state.appClicks[eID] += 1
    // vue 的雷： object & array 的變化，需要用下面方式才會觸發 computed / getters
    // ref: https://github.com/vuejs/vuex/issues/1311
    Vue.set(state.appClicks, eID, state.appClicks[eID])
    // 存入 cookies
    VueCookies.set('kkAppClicks', state.appClicks)
    // 存入殼裡
    deviseFunction('SetSP', `{"spName":"appClicks", "spValue": ${ JSON.stringify(state.appClicks) }}`, '')
  },
  // 存入會員擁有的企業 app 使用者資料 (重複的就刷新點數資訊, 卡片資訊)
  setVipUserData(state, app) {
    const userData = state.vipUserData.find(item => item.EnterPriseID === app.EnterPriseID)
    const notFound = userData === undefined

    if (notFound) { 
      state.vipUserData.push(app) 
    }
    if (!notFound) {
      userData.cardPoint = app.cardPoint
      userData.CardTypeCode = app.CardTypeCode
      userData.CardTypeName = app.CardTypeName
      userData.CardFacePhoto = app.CardFacePhoto
    }
  },
  setVipUserLoadInit() { state.vipAppsLoaded = false },
  setVipUserLoaded() { state.vipAppsLoaded = true },
  // 設定 cssTouch
  setCssTouch(state, status){ state.cssTouch = status },
  // 設定 商城目前scrollTop
  setMallScrollTop(state, value){ state.mallData.mallScrollTop = value },
  // 設定 商城,首頁,目前scrollTop
  setMallHomeScrollTop(state, value){ state.mallData.mallHomeScrollTop = value },
  // 設定是否在殼裡面
  setIsDeviseApp(state, status) { state.isDeviseApp = status },
  // 『讀取中...』 切換
  setLoading(state, status) { state.isLoading = status },
  //暫存卡包企業首頁進商城主橫幅
  setMarketBanner(state, data) { state.marketBanner = data },
  // 『顯示手動輸入 QRcode 表格』切換
  setInputQRCode(state, status) { state.showInputQRCode = status },
  // 設定彈跳視窗的資料 (顯示 / 隱藏)
  setCustomModal(state, { type, text, cancelText, confirmText, 
    confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
    confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg,
    resolve, reject }) {
    cancelText = cancelText || '取消'
    confirmText = confirmText || '確認'

    state.customModal = { type, text, cancelText, confirmText,
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg}
    
    if (resolve && reject) state.promise = { resolve, reject }
  },
  // 存入已綁定的企業 app 列表
  setVipApps(state, payload) { state.vipApps = payload },
  // 存入手機簡訊認證的設定
  setSMS_Config(state, data) { state.SMS_Config = Object.assign({}, state.SMS_Config, data) },
  // 存入基本資訊
  setBaseInfo(state, data) { 
    const tmpInfo = Object.assign({}, state.baseInfo, data);
    state.baseInfo = null;
    state.baseInfo = Object.assign({}, tmpInfo) 
  },
  // 更新會員資訊
  setUserData(state, data) { state.userData = data },
  //Api service url(第一次登入時暫放)
  setLoginInfoRtAPIUrl(state, data){    
    state.userData.RtAPIUrl = data.RtAPIUrl || "";
  },
  // 存入會員登入資訊
  setLoginInfo(state, payload) {
    // payload sample: {data: data, rememberMe: true, pwd: 'xxx'}
    const data = payload.data
    //console.log('===> data =>', data)
    //console.log('===> payload.pwd =>' + payload.pwd)
    //console.log('===> payload.rememberMe =>' + payload.rememberMe)

    // 存入 cookies
    if (payload.rememberMe) {
      console.log('===> is payload.rememberMe')
      if (typeof(JSInterface) == 'undefined') {
        console.log('setLoginInfo ===> in browser => save account to cookies')
        VueCookies.set('kkMAC', data.mac)
        VueCookies.set('kkTokenkey', data.Tokenkey)
        VueCookies.set('kkUserData', data)
        VueCookies.set('kkUserCode', data.Account)
        VueCookies.set('kkUserPwd', payload.pwd)  
      }
    }
    // 存入 store
    
    state.userData = data    
    state.member.mac = data.mac
    state.member.Tokenkey = data.Tokenkey
    state.member.code = data.Account
    state.member.pwd = payload.pwd

    // 如果是龍海 app, 依據登入參數導向指定頁面
    if (data.EnterPriseID === "53239538") setRedirectPage()
  },
  // 清除企業 app 的會員資料
  clearAppMemberData(state) { state.memberData = rawRemberData() },
  // 清除企業 app 的公開資料
  clearAppPublicData(state) { state.publicData = rawPublicData() },
  // 會員登出
  setLogout(state) {
    VueCookies.remove('kkJWT')
    VueCookies.remove('kkMAC')
    VueCookies.remove('kkTokenkey')
    VueCookies.remove('kkUserData')
    VueCookies.remove('kkUserCode')
    VueCookies.remove('kkUserPwd')
    VueCookies.remove('kkUserJWT')

    state.vipApps = []
    state.userData = {}
    state.vipUserData = []
    // 清除會員基本資訊
    state.member.mac = ''
    state.member.Tokenkey = ''
    state.member.code = ''
    state.member.pwd = ''

    // === 此console不可以移除 === start
    let msg123 = '===> state.member =>' + JSON.stringify(state.member)
      + '<=== cookie => '
      + 'tokenkey=>' + VueCookies.get('kkTokenkey')
      + '<= kkJWT =>' + VueCookies.get('kkJWT')
      + '<= kkMAC =>' + VueCookies.get('kkMAC')
      + '<= kkUserData =>' + VueCookies.get('kkUserData')
      + '<= kkUserCode =>' + VueCookies.get('kkUserCode')
      + '<= kkUserPwd =>' + VueCookies.get('kkUserPwd')
      + '<= kkUserJWT =>' + VueCookies.get('kkUserJWT')

    console.log(msg123)
    // alert(msg123)
    // === 此console不可以移除 === end
  },
  // 存入企業 app 的 LOGO 資訊
  setAppLogo(state, data) { state.publicData.logoLoaded = true ;state.publicData.logo = data },
  // 存入企業 app 的 LOGO 資訊 - 五互
  setAppLogoWuhu(state, data) { state.baseInfo.publicData.logoLoaded = true ;state.baseInfo.publicData.logo = data },
  // 存入我的預約
  setMyBookList(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
      }) 
    }
    state.baseInfo.publicData.myBookList = data
  },
  // 存入我的預約
  setMyBookStatusList(state, data) { 
    state.baseInfo.publicData.myBookStatusList = data
  },
  // 存入企業 app 的 db connect 資訊
  setAppConnect(state, data) { const connect = data || '9001' ; state.publicData.connect = connect },
  // 存入首頁主畫面可滑動的 Banners
  setIndexSlideBanners(state, data) { state.publicData.index.slideBanners = data },
  // 存入首頁主畫面可滑動的 Banners - 五互
  setIndexSlideBannersWuhu(state, data) { state.baseInfo.publicData.index.slideBanners = data },
  // 存入首頁固定的副 Banner
  setIndexSubBanner(state, data) { state.publicData.index.subBanner = data },
  // 五互 - 存入首頁固定的副 Banner
  setIndexSubBannerWuhu(state, data) { state.baseInfo.publicData.index.subBanner = data },
  // 存入企業門店清單
  setStoreListData(state, payload) {
    const {stores, MainEnterPriseID, MainConnect} = payload
    state.publicData.MainEnterPriseID = MainEnterPriseID
    state.publicData.MainConnect = MainConnect
    state.publicData.storeList = stores
  },
  // 存入企業門店清單
  setStoreListDataWuhu(state, payload) {
    let {stores/*, MainEnterPriseID, MainConnect*/} = payload
    // state.publicData.MainEnterPriseID = MainEnterPriseID
    // state.publicData.MainConnect = MainConnect
    
    state.baseInfo.publicData.storeList = stores
  },
  // 存入企業自取門店清單
  setStoreListDataFtype(state, payload) {
    let {stores/*, MainEnterPriseID, MainConnect*/} = payload
    // state.publicData.MainEnterPriseID = MainEnterPriseID
    // state.publicData.MainConnect = MainConnect
    
    state.baseInfo.publicData.storeFtype = stores
  },
  // 存入品牌資訊
  setBrandInfo(state, data) { if (data) state.publicData.brandInfo = data },
  // 存入最新消息的資料
  setNewsData(state, data) { if (!data.ErrorCode) state.publicData.news.data = data },
  // 存入最新消息的資料
  setNewsDataWuhu(state, data) { if (!data.ErrorCode) state.baseInfo.publicData.news.data = data },
  // 存入最新消息的分類
  setNewsTypes(state, data) { if (!data.ErrorCode) state.publicData.news.types = data },
  // 存入菜單的大類資料
  setMenuMainTypeListData(state, payload) { state.publicData.menu.mainType.find(item => {return item.code === payload.code}).list = payload.data},
  //存入全部小類下商品
  setFoodmarketData(state, data) {
    const foodCheck=(foods,isSetFood)=>{return foods.map(p_food=>{
      p_food.ID = p_food.ID || p_food.MainID;  //品項ID
      p_food.Price = p_food.CurPrice;    //原價
      if (isSetFood && !state.mallData.foodInfo[p_food.ID]) state.mallData.foodInfo[p_food.ID] = p_food; 
      // p_food.Price1 = (p_food.Price1 != undefined?p_food.Price1:9999);  //售價

      return p_food;
    });}
    
    if (Array.isArray(data.foods)) {
      state.mallData.iv_foods[data.kindID] = foodCheck(data.foods,true);
      
      const findKind = (et) => et.ID == data.kindID;
      var kind_index = state.mallData.foodkind.findIndex(findKind);
      state.mallData.foodkind[kind_index]["foods"] = foodCheck(data.foods);
      // console.log("setFoodmarketData",data.kindID,state.mallData);   
    }

  },
  setMenuMainTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.mainType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.mainType = state.publicData.menu.mainType.concat( { code: data.code, name: data.name, list: [] } )
  },
  // 存入菜單的小類資料
  setMenuSubTypeListData(state, payload)  {state.publicData.menu.subType.find(item => {return item.code === payload.code}).list = payload.data},
  setMenuSubTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.subType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.subType = state.publicData.menu.subType.concat( { code: data.code, name: data.name, list: [] } )
  },
  // 存入會員卡資訊
  setMemberVipData(state, data) {
    state.memberData.vip_info = data.querydata_return_info.vip_info
    state.memberData.card_info = data.querydata_return_info.card_info
    state.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    state.memberData.expiredPoints_section = data.ExpiredPoints_section // 年度總積點資料
  },
  //存入綠界Info
  setEcPayData(state, data) {
    console.log("ttc>>>>",state, data);
  },
  //特約商基本資料
  setSpecShopWuhu(state, data) {
    state.baseInfo.memberData.specShopList = data;
  },
  //特約商成立後的訂單
  setSpecOrderWuhu(state, data) {
    state.baseInfo.memberData.specOrder = data;    
  },
  //特約商購物車
  setSpecCartWuhu(state, data) {
    state.baseInfo.memberData.specCart = data;
  },
  //退貨 / 退款原因表 //RefundReason
  setReasonWuhu(state, data) {
    data = data || [];
    if (Array.isArray(data)){
      const p_reason = data.filter(rea=>{ return rea.isAppSet == true});
      state.baseInfo.memberData.appReason = p_reason.map((reaCode,index)=>{
        return {code:'reason_'+index,text:reaCode.RefundReason || ''};
      });
    }else{
      state.baseInfo.memberData.appReason = [];
    }
    
  },
  //訂閱制清單
  setSubscription(state, data) {
    state.baseInfo.memberData.subscriptionList = data;
  },
  setIsVipDataLoad(state, val) {
    state.baseInfo.memberData.isVipDataLoad = val;
  },
  // 存入會員卡資訊 - 五互
  setMemberVipDataWuhu(state, data) {
    if (!data || !data.querydata_return_info) return;    
    state.baseInfo.memberData.vip_info = data.querydata_return_info.vip_info
    state.baseInfo.memberData.card_info = data.querydata_return_info.card_info
    state.baseInfo.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    
    // === only for test start ===
    // data.ExpiredPoints_section = [
    //   {
    //     ShopID:"TX07",
    //     ShopName:"香繼光測試機B",
    //     Points:"68.00",
    //     ExpiredDate:"2020-12-31"
    //   },
    //   {
    //     ShopID:"TX07",
    //     ShopName:"香繼光測試機B",
    //     Points:"6.00",
    //     ExpiredDate:"2021-12-31"
    //   }]
    // === only for test end ===

    // === 排序 start ===
    let expiredPoints_section = data.ExpiredPoints_section
    if (expiredPoints_section && expiredPoints_section.length>0) {
      if (expiredPoints_section.length>1) {
        // 排序: 由小到大
        expiredPoints_section.sort(function (a, b) {
          if (a.ExpiredDate > b.ExpiredDate) {
            return 1
          } else {
            return -1
          }
        })
      }
    }
    // === 排序 end ===

    state.baseInfo.memberData.expiredPoints_section = expiredPoints_section // 年度總積點資料
  },
  
  // 設定 卡包,目前scrollTop
  setCardListScrollMap(state, value){ state.cardListScrollMap = value },
  // 存入卡片條碼 QRcode
  setCardQRCodeData(state, data) { state.cardQRCode = data },
  // 設定簡訊驗證碼
  setSMSreCode(state, data) { state.SMS_Config.reCode = data },
  // 存入會員的購買交易紀錄
  setMemberPurchaseRecordData(state, data) { state.memberData.purchaseRecord = data },
  // 存入優惠卷列表資訊
  setMemberTicketListData(state, data) {state.memberData.ticketList = data},
  // 存入優惠卷列表資訊 - 五互
  setMemberTicketListDataWuhu(state, data) {
    // === only for test start ===
  //   data = [{
  //     "GID":"3eb66598-6db5-4fad-adcb-af5b067a1fad",
  //     "TicketInfoID":"fc6a2f81-ca85-409b-b773-efee2a3fa9f6",
  //     "TradeNum":"f32c37a9-58b4-47ef-9cb0-641fc2b29177",
  //     "MemberNO":"0919142312",
  //     "CardNO":"0919142312",
  //     "CardID":"0919142312",
  //     "TradeRuleCode":"",
  //     "TicketTypeCode":"CR20030901",
  //     "TicketTypeName":"(獨家優惠)金子半之助香香雞天丼優惠券",
  //     "StartTicketCount":"1",
  //     "StartTicketTotal":"0",
  //     "TicketCount":"1",
  //     "TicketPrice":"0",
  //     "TicketTotal":"0",
  //     "TicketBgnNO":"fc6a2f81-ca85-409b-b773-efee2a3fa9f6",
  //     "TicketEndNO":"fc6a2f81-ca85-409b-b773-efee2a3fa9f6",
  //     "TicketBeginDate":"0001-01-01T00:00:00",
  //     "TicketExpiredDate":"2020-03-31T00:00:00",
  //     "GenTradetime":"2020-03-11T10:32:26",
  //     "Operator":"",
  //     "MustInputTicketNO":false,
  //     "MAC":"0ba1e2595c34b36505292b9f1ff5e4c3",
  //     "OrderMinMoney":"0",
  //     "FoodMaxMoney":"0",
  //     "TicketFlag":"2",
  //     "UseMaxCount":"0",
  //     "OrderID":"",
  //     "OrderNO":"",
  //     "TicketDisCount":"0",
  //     "CardTradeNum":"ecb27c3a-19dd-4a65-b977-ddb0102faa0e",
  //     "CXTradeNum":"",
  //     "TicketNumCount":"0",
  //     "GenReason":"",
  //     "Remark":"領用[20200309](聯名)香香雞天丼優惠券(42544)",
  //     "EnterPriseID":"24435384",
  //     "ImageUrl":"/Images/TicketBanner/24435384/CR20030901.jpg",
  //     "PublishGID":"",
  //     "DrawGID":"",
  //     "PutGID":"",
  //     "CaceldGID":"",
  //     "ExtendGID":"",
  //     "TIfrom_GID":"",
  //     "TIto_GID":"",
  //     "VipMinPoint":"0",
  //     "BirthDayRuleType":"0"
  //   },
  //   {
  //     "GID":"4d479d84-f7e0-48db-9ef5-b89bbbe73c7f",
  //     "TicketInfoID":"3075875a-dc06-4cad-92b4-fb39bb65a1b3",
  //     "TradeNum":"addc4841-1959-4e8e-be59-c3a0cf2b5eda",
  //     "MemberNO":"0919142312",
  //     "CardNO":"0919142312",
  //     "CardID":"0919142312",
  //     "TradeRuleCode":"",
  //     "TicketTypeCode":"CR20020502",
  //     "TicketTypeName":"甜而不辣八折券",
  //     "StartTicketCount":"1",
  //     "StartTicketTotal":"0",
  //     "TicketCount":"1",
  //     "TicketPrice":"0",
  //     "TicketTotal":"0",
  //     "TicketBgnNO":"3075875a-dc06-4cad-92b4-fb39bb65a1b3",
  //     "TicketEndNO":"3075875a-dc06-4cad-92b4-fb39bb65a1b3",
  //     "TicketBeginDate":"0001-01-01T00:00:00",
  //     "TicketExpiredDate":"2020-04-30T00:00:00",
  //     "GenTradetime":"2020-03-12T15:25:26",
  //     "Operator":"",
  //     "MustInputTicketNO":true,
  //     "MAC":"077e8b51ee6dce2548a82d5068233112",
  //     "OrderMinMoney":"0",
  //     "FoodMaxMoney":"0",
  //     "TicketFlag":"3",
  //     "UseMaxCount":"0",
  //     "OrderID":"",
  //     "OrderNO":"",
  //     "TicketDisCount":"80",
  //     "CardTradeNum":"cc437ee9-75cd-421d-9351-bdce2381a78f",
  //     "CXTradeNum":"",
  //     "TicketNumCount":"0",
  //     "GenReason":"",
  //     "Remark":"*******935轉贈:2020-03-12",
  //     "EnterPriseID":"24435384",
  //     "ImageUrl":"/Images/TicketBanner/24435384/CR20020502.png",
  //     "PublishGID":"",
  //     "DrawGID":"",
  //     "PutGID":"",
  //     "CaceldGID":"",
  //     "ExtendGID":"",
  //     "TIfrom_GID":"",
  //     "TIto_GID":"",
  //     "VipMinPoint":"0",
  //     "BirthDayRuleType":"0"
  // }]
  // === only for test end ===
    state.baseInfo.memberData.ticketList = data
  },
  // 存入已贈送的優惠卷列表資訊
  setMemberGaveTicketListData(state, data) {state.memberData.gaveTicketList = data},
  // 存入已贈送的優惠卷列表資訊 - 五互
  setMemberGaveTicketListDataWuhu(state, data) {
    // data = [
    //   {
    //     "RowNo":"1",
    //     "TradeTime":"2020-03-12 17:20:49",
    //     "ImageUrl":"/Images/TicketBanner/24435384/CR19053103.png",
    //     "TicketFlag":2,
    //     "TicketTypeName":"蒜脆杏鮑菇 兌換券"
    //   },
    //   {
    //     "RowNo":"2",
    //     "TradeTime":"2020-03-12 17:00:12",
    //     "ImageUrl":"/Images/TicketBanner/24435384/Test190305.jpg",
    //     "TicketFlag":2,
    //     "TicketTypeName":"測試用積分換飲品券"
    //   },
    //   {
    //     "RowNo":"3",
    //     "TradeTime":"2019-11-24 22:39:07",
    //     "ImageUrl":"/Images/TicketBanner/24435384/CR19091201.jpg",
    //     "TicketFlag":2,
    //     "TicketTypeName":"冷泡茶系列(任選)兌換券"
    //   },
    //   {
    //     "RowNo":"4",
    //     "TradeTime":"2019-10-14 14:00:41",
    //     "ImageUrl":"/imagesPic.jpg",
    //     "TicketFlag":1,
    //     "TicketTypeName":"測試用滿額現金券" 
    //   }
    // ]
    state.baseInfo.memberData.gaveTicketList = data
  },
  // 存入尚未能使用的優惠卷資料
  setMemberNotAvailableTickets(state, data) {state.memberData.notAvailableTickets = data},
  // 存入尚未能使用的優惠卷資料 - 五互
  setMemberNotAvailableTicketsWuhu(state, data) {
    // data = [{
    //   "GID":"3eb66598-6db5-4fad-adcb-af5b067a1fae",
    //   "TicketInfoID":"fc6a2f81-ca85-409b-b773-efee2a3fa9f6",
    //   "TradeNum":"f32c37a9-58b4-47ef-9cb0-641fc2b29177",
    //   "MemberNO":"0919142312",
    //   "CardNO":"0919142312",
    //   "CardID":"0919142312",
    //   "TradeRuleCode":"",
    //   "TicketTypeCode":"CR20030901",
    //   "TicketTypeName":"(獨家優惠)金子半之助香香雞天丼優惠券",
    //   "StartTicketCount":"1",
    //   "StartTicketTotal":"0",
    //   "TicketCount":"1",
    //   "TicketPrice":"0",
    //   "TicketTotal":"0",
    //   "TicketBgnNO":"fc6a2f81-ca85-409b-b773-efee2a3fa9f6",
    //   "TicketEndNO":"fc6a2f81-ca85-409b-b773-efee2a3fa9f6",
    //   "TicketBeginDate":"0001-01-01T00:00:00",
    //   "TicketExpiredDate":"2020-03-31T00:00:00",
    //   "GenTradetime":"2020-03-11T10:32:26",
    //   "Operator":"",
    //   "MustInputTicketNO":false,
    //   "MAC":"0ba1e2595c34b36505292b9f1ff5e4c3",
    //   "OrderMinMoney":"0",
    //   "FoodMaxMoney":"0",
    //   "TicketFlag":"2",
    //   "UseMaxCount":"0",
    //   "OrderID":"",
    //   "OrderNO":"",
    //   "TicketDisCount":"0",
    //   "CardTradeNum":"ecb27c3a-19dd-4a65-b977-ddb0102faa0e",
    //   "CXTradeNum":"",
    //   "TicketNumCount":"0",
    //   "GenReason":"",
    //   "Remark":"領用[20200309](聯名)香香雞天丼優惠券(42544)",
    //   "EnterPriseID":"24435384",
    //   "ImageUrl":"/Images/TicketBanner/24435384/CR20030901.jpg",
    //   "PublishGID":"",
    //   "DrawGID":"",
    //   "PutGID":"",
    //   "CaceldGID":"",
    //   "ExtendGID":"",
    //   "TIfrom_GID":"",
    //   "TIto_GID":"",
    //   "VipMinPoint":"0",
    //   "BirthDayRuleType":"0"
    // },
    // {
    //   "GID":"4d479d84-f7e0-48db-9ef5-b89bbbe73c7g",
    //   "TicketInfoID":"3075875a-dc06-4cad-92b4-fb39bb65a1b3",
    //   "TradeNum":"addc4841-1959-4e8e-be59-c3a0cf2b5eda",
    //   "MemberNO":"0919142312",
    //   "CardNO":"0919142312",
    //   "CardID":"0919142312",
    //   "TradeRuleCode":"",
    //   "TicketTypeCode":"CR20020502",
    //   "TicketTypeName":"甜而不辣八折券",
    //   "StartTicketCount":"1",
    //   "StartTicketTotal":"0",
    //   "TicketCount":"1",
    //   "TicketPrice":"0",
    //   "TicketTotal":"0",
    //   "TicketBgnNO":"3075875a-dc06-4cad-92b4-fb39bb65a1b3",
    //   "TicketEndNO":"3075875a-dc06-4cad-92b4-fb39bb65a1b3",
    //   "TicketBeginDate":"0001-01-01T00:00:00",
    //   "TicketExpiredDate":"2020-04-30T00:00:00",
    //   "GenTradetime":"2020-03-12T15:25:26",
    //   "Operator":"",
    //   "MustInputTicketNO":true,
    //   "MAC":"077e8b51ee6dce2548a82d5068233112",
    //   "OrderMinMoney":"0",
    //   "FoodMaxMoney":"0",
    //   "TicketFlag":"3",
    //   "UseMaxCount":"0",
    //   "OrderID":"",
    //   "OrderNO":"",
    //   "TicketDisCount":"80",
    //   "CardTradeNum":"cc437ee9-75cd-421d-9351-bdce2381a78f",
    //   "CXTradeNum":"",
    //   "TicketNumCount":"0",
    //   "GenReason":"",
    //   "Remark":"*******935轉贈:2020-03-12",
    //   "EnterPriseID":"24435384",
    //   "ImageUrl":"/Images/TicketBanner/24435384/CR20020502.png",
    //   "PublishGID":"",
    //   "DrawGID":"",
    //   "PutGID":"",
    //   "CaceldGID":"",
    //   "ExtendGID":"",
    //   "TIfrom_GID":"",
    //   "TIto_GID":"",
    //   "VipMinPoint":"0",
    //   "BirthDayRuleType":"0"
    // },
    // {
    //   "GID":"9ea28232-45c6-4103-824f-05e0b00950e7",
    //   "TicketInfoID":"41dafb71-e738-4b54-b3ed-4f9ce003bffe",
    //   "TradeNum":"769fbb48-c7e0-415a-bc11-1aa72cc6f214",
    //   "MemberNO":"0919142312",
    //   "CardNO":"0919142312",
    //   "CardID":"0919142312",
    //   "TradeRuleCode":"",
    //   "TicketTypeCode":"CR20020501",
    //   "TicketTypeName":"炸雞雙翅雙人餐優惠券",
    //   "StartTicketCount":"1",
    //   "StartTicketTotal":"35",
    //   "TicketCount":"1",
    //   "TicketPrice":"35",
    //   "TicketTotal":"35",
    //   "TicketBgnNO":"41dafb71-e738-4b54-b3ed-4f9ce003bffe",
    //   "TicketEndNO":"41dafb71-e738-4b54-b3ed-4f9ce003bffe",
    //   "TicketBeginDate":"0001-01-01T00:00:00",
    //   "TicketExpiredDate":"2020-04-30T00:00:00",
    //   "GenTradetime":"2020-02-07T22:06:26",
    //   "Operator":"",
    //   "MustInputTicketNO":true,
    //   "MAC":"bb829deb5351e587adb5e4f8bae39cc4",
    //   "OrderMinMoney":"0",
    //   "FoodMaxMoney":"0",
    //   "TicketFlag":"2",
    //   "UseMaxCount":"0",
    //   "OrderID":"",
    //   "OrderNO":"",
    //   "TicketDisCount":"0",
    //   "CardTradeNum":"ddbf4ae9-fd77-49d1-9399-335e1c5a7f45",
    //   "CXTradeNum":"",
    //   "TicketNumCount":"0",
    //   "GenReason":"",
    //   "Remark":"領用[20200207]登入領情人節套餐券(45800)",
    //   "EnterPriseID":"24435384",
    //   "ImageUrl":"/Images/TicketBanner/24435384/CR20020501.png",
    //   "PublishGID":"",
    //   "DrawGID":"",
    //   "PutGID":"",
    //   "CaceldGID":"",
    //   "ExtendGID":"",
    //   "TIfrom_GID":"",
    //   "TIto_GID":"",
    //   "VipMinPoint":"0",
    //   "BirthDayRuleType":"0" }]
    state.baseInfo.memberData.notAvailableTickets = data
  },
  // 存入會員的積點紀錄
  setMemberExpiredPointsData(state, data) { state.memberData.expiredPoints = data },
  // 存入會員的積點紀錄
  setMemberExpiredPointsDataWuhu(state, data) { state.baseInfo.memberData.expiredPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointsDataWuhu(state, data) { state.baseInfo.memberData.consumerPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointDetailWuhu(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
        item.orderDetail = null // 該筆訂單的購物清單
      })
    }

    state.baseInfo.memberData.consumerPointDetail = data 
  },
  // 存入會員的單筆訂單紀錄
  setOneOrderDetail(state, data) {
    if (data) {
      let currentItem = state.baseInfo.memberData.consumerPointDetail[data.arrayIndex] 
      if (currentItem) {
        currentItem.orderDetail = data.orderDetail  
      }  
    }
  },
  // 存入優惠卷明細資訊 e.g. 規則條款、期限、適用範圍 ... etc
  setMemberTicketInfoData(state, data) {
    const ticket = state.baseInfo.memberData.ticketInfo
    // 規則代號
    if (data.currentTypeCode) ticket.currentTypeCode = data.currentTypeCode
    if (data.currentRuleCode) ticket.currentRuleCode = data.currentRuleCode
    // 使用規則、條款
    if (data.ruleData)   ticket.ruleData = data.ruleData
    // 適用商品
    if (data.usageFoods) ticket.usageFoods = data.usageFoods
    // 適用門店
    if (data.usageShops) ticket.usageShops = data.usageShops
  },
  // 存入優惠卷條碼 QRcode
  setTicketQRCodeData(state, data) {
    state.baseInfo.memberData.ticketInfo.currentTicketGID    = data.gid
    state.baseInfo.memberData.ticketInfo.currentTicketQRcode = data.image
  },
  // 存入殼的版本是否需要更新
  SetDeviseVersionIsDiff(state, status) {
    const value = (status === '1') ? true : false
    state.baseInfo.DeviseVersionIsDiff = value
  }, 
   // 存入購物車 
  setShopCart(state, data) { state.mallData.shopping_cart = data },
   // 新增購物車 
  addShopCart(state, data) { state.mallData.shopping_cart.push(data) },
  // 付款方式清單
  setCheckKind(state, data) { state.mallData.checkKind_list = data },
  // 商城觸發UI更新用
  turnShopCart(state) {state.mallData.isCarUI = !state.mallData.isCarUI},
  // 商城分組商品更新
  groupFood(state, data) {state.mallData.home[data.fIndex]["type"] = data.GroupItems;}, 
   // 存入商品詳情頁 
  setShopNowItem(state, data) {state.mallData.nowItem = data},
   // 存入商品詳情頁,規格
  setShopNowItemStyle(state, data) {state.mallData.nowItem.style = data},
  // 商城存入小類商品 
  setShopKindFoods(state, data) {state.mallData.kind[data.index]["foods"] = data.foods},
  // 商城存入訂單記錄
  setShopRecord(state, data) { state.mallData.orders = data },
  // 商城存入訂單記錄,分頁
  setShopRecordPage(state, data) { state.mallData.ordersPage = data },
  // 商城存入訂單記錄,分頁
  setShopUndRecordPage(state, data) { state.mallData.unOrdersPage = data },
  // 存入特殊商品下拉數據
  setSpecLimit(state, data) {
    state.mallData.foodSpecLimitData={};
    if (Array.isArray(data)) data.map(arr=>{
      state.mallData.foodSpecLimitData[arr.id] = arr.text;
    });
    
  },
  setPayStatus(state, data) {    
    if(Array.isArray(data)){
      data.map((p_status)=>{
        const p_key =p_status.id.toString();
        state.mallData.payStatus[p_key] = p_status.text;
      });
      
    }
  },
  // 商城存入品項規則
  setFoodSpecLimit(state, data) { 
    state.mallData.foodSpecLimit = data;
    state.mallData.foodSpecLimitGp = {};
    const gp_spec = groupBy(state.mallData.foodSpecLimit,"LimitID");
    if (gp_spec.length > 0){
      gp_spec.map((sub_arr)=>{
        if (sub_arr.length > 0){
          const p_key = sub_arr[0]["LimitID"];
          state.mallData.foodSpecLimitGp[p_key]={};
          state.mallData.foodSpecLimitGp[p_key]["Name"] = sub_arr[0]["LimitName"];
          state.mallData.foodSpecLimitGp[p_key]["foodIDs"] = sub_arr.map((sub)=>{return sub.MainID});
        }
        
      });
      
    }
    
    
  },
  // 商城小類頁品項排序
  setSortPriceType(state, data) {state.mallData.sortPriceType = data},
  // 商城小類頁scrollListener
  setShoplistenELe(state, data) {state.mallData.listenEL = data},
  // 存入單筆已經完成訂單記錄
  setSingleFinish(state, data) {
    state.mallData.singleFinish = data
  },
  // 商城清除暫存
  clsShopMall(state) {
    state.mallData.foodkind2 = [];  //大類
    state.mallData.foodkind = [];   //小類        
    state.mallData.iv_foods = {}; //小類下的所有品項
    state.mallData.iv_styles = {}; //品項下的所有規格
    state.mallData.foodInfo = {}; //by foodID 品項基本資料
  },
  setBecomeApp(state) {
    state.mallData.becomeApp = !state.mallData.becomeApp
  },
  initQrCodeItem(state){
    //由掃瞄生成的品項
    state.mallData.barcodeItem = [{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}];
  },
  
}

const getters = {
  getBaseUrl : () => {
    return baseUrl
  },
  fontSizeBase: state => {
    let fontSize = state.fontSizeBase
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    return parseFloat(fontSize)
  },
  // 站台 router
  router: state => {
    if (state.appSite === 'wuhulife') return routerWuhulife
    return routerKakar
  },
  // 是否登入
  isLogin: state => {
    return (state.member.code !== '' && state.member.mac !== '' && state.member.Tokenkey !== '')
  },
  // 卡卡的 requestBody
  kakarBody: state => {
    const {mac, code} = state.member
    const {isFrom, EnterpriseID, FunctionID} = state.baseInfo
    return { FunctionID, EnterpriseID, isFrom, mac, Account: code, }
  },
  // 企業 app 的 requestBody
  appBody: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, isFrom, EnterPriseID} = state.currentAppUser
    const basicBody = {
      mac, isFrom,
      EnterpriseID: EnterPriseID,
      Account: state.member.code,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBodyWuhu: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }
    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同
      Account: state.member.code,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBodyWuhuEncode: state => (body, paramBody) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }
    
    // === param start ===
    const paramInputBody = (paramBody || {})
    let paramData = {
      Account: btoa(state.member.code)
    }
    let paramEndData = Object.assign({}, paramData, paramInputBody)
    // === param end ===

    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同
      Account: state.member.code, // 2020-04-22 add
      param: paramEndData,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 目前的企業列表 (使用 state.appFilter 篩選 + 依據點擊次數排序)
  currentAppUsers: state => {
    let appUsers = state.vipUserData
    // 依據點擊次數排序
    appUsers = appUsers.sort((a, b) => {
      const aClicks = state.appClicks[a.EnterPriseID] || 0
      const bClicks = state.appClicks[b.EnterPriseID] || 0
      if (aClicks > bClicks) return -1
      if (aClicks < bClicks) return 1
      if (aClicks === bClicks) return 0
    })
    // 使用 state.appFilter 篩選
    if (state.appFilter === '') return appUsers
    const eIDs = state.vipApps.filter(item => {
      const filter  = state.appFilter.trim().toLowerCase()
      const eID     = item.EnterpriseID.trim().toLowerCase()
      const isFrom  = item.isFrom.trim().toLowerCase()
      const AppName = item.AppName.trim().toLowerCase()
      return RegExp(filter).test(eID) || RegExp(filter).test(isFrom) || RegExp(filter).test(AppName)
    }).map(item => item.EnterpriseID)
    return state.vipUserData.filter(item => eIDs.find(eID => (eID === item.EnterPriseID)) )
  },
  currentAppUsersWuhu: state => {
    let appUsers = state.vipUserData.sort((a, b) => {
      if (a.EnterPriseID < b.EnterPriseID) {
        return 1;
      }
      if (a.EnterPriseID > b.EnterPriseID) {
        return -1;
      }
    })
    // 依據點擊次數排序
    // appUsers = appUsers.sort((a, b) => {
    //   const aClicks = state.appClicks[a.EnterPriseID] || 0
    //   const bClicks = state.appClicks[b.EnterPriseID] || 0
    //   if (aClicks > bClicks) return -1
    //   if (aClicks < bClicks) return 1
    //   if (aClicks === bClicks) return 0
    // })
    
    // 使用 state.appFilter 篩選
    if (state.appFilter === '') return appUsers
    const eIDs = state.vipApps.filter(item => {
      const filter  = state.appFilter.trim().toLowerCase()
      const eID     = item.EnterpriseID.trim().toLowerCase()
      const isFrom  = item.isFrom.trim().toLowerCase()
      const AppName = item.AppName.trim().toLowerCase()
      return RegExp(filter).test(eID) || RegExp(filter).test(isFrom) || RegExp(filter).test(AppName)
    }).map(item => item.EnterpriseID)
    return appUsers.filter(item => eIDs.find(eID => (eID === item.EnterPriseID)) )
  },
  // 目前的企業 App
  currentApp: state => {
    return state.vipApps.find(item => item.EnterpriseID === state.currentAppUser.EnterPriseID)
  },
  connect: (state) => {
    return state.publicData.connect
  },
  // 企業 app 的 memberApiUrl
  appMemberApiUrl: (state, getters) => {
    return getters.currentApp.apiip
  },
  // 企業 app 的 memberApiUrl - 五互
  appMemberUrlWuhu: (state) => {
    return state.api.memberUrl
  },
  // 企業 app 的 預約資料 - 五互
  appBookUrlWuhu: (state) => {
    return state.api.bookUrl
  },
  // 企業 app 的 預約token資料 - 五互
  appTokenUrlWuhu: (state) => {
    return state.api.tokenUrl
  },
  // 企業 app 的 publicApiUrl
  appPublicApiUrl: (state, getters) => {
    // return getters.currentApp.apiip.replace('/public/AppVIP.ashx', '/public/AppNCW.ashx')
    return getters.currentApp.apiip.substring(0, (getters.currentApp.apiip.indexOf('/public')+1))
      + '/public/AppNCW.ashx'
  },
  // 靜態檔的 url
  srcUrl: (state, getters) => {
    // getters.currentApp.apiip.replace('/public/AppDataVIP.ashx', '')
    return getters.currentApp.apiip.substring(0, (getters.currentApp.apiip.indexOf('/public')+1))
  },

  appVipUrl: (state) => {
    //會員資料存取,例:收件人存取 by 20200225
    return state.api.vipUrl
  },
  // 取手機的螢幕亮度值
  deviseBrightness: state => {
    if (state.baseInfo.brightness <= 70) return 120
    return state.baseInfo.brightness
  },
  //  五互[我的預約]歷史紀錄
  getMyBookList: (state) => {
    return state.baseInfo.publicData.myBookList
  },
  //  五互[我的預約]狀態清單
  getMyBookStatusList: (state) => {
    return state.baseInfo.publicData.myBookStatusList
  } //,
  // getCustomModal: (state) => {
  //   return state.customModal
  // }
}

const actions = {
  /* 卡卡資料相關 */

  // init 時執行所需的 action
  async fetchInitData() {
    // 1. 跟殼取資料
    await ensureDeviseSynced()
  },
  // 確認會員帳號是否未註冊
  async ensureMemberCodeNotRegistered({state,getters}, account) {
    const apiBody = { 
      act: "memberchk", 
      Account: account,
    }
		const url = state.api.noTokenUrl
			,body 	= getters.appBodyWuhu(apiBody)
			,update = await fetchData({url, body})
    return new Promise(resolve => resolve(update))
  },
  // (卡卡) 會員註冊
  async ensureMemberRegister({state}, payload){
    const url = state.api.memberUrl
    const body = Object.assign({}, { act: "memberreg", "isFrom": "kakar", "EnterpriseID": "kakar" }, payload)
    const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
    const {data} = await axios.post( url, body, head )
    return new Promise(resolve => resolve(data))
  },

  // login by child
  async childLogin({state, getters}) {
    let url = getters.currentApp.apiip
    // == 之後秋林api改成回AppDataVIP.ashx之後可以刪除這段 == start
    if (url && url.indexOf('AppDataVIP.ashx')<0) {
      url = getters.appTokenUrlWuhu
    }
    // == 之後秋林api改成回AppDataVIP.ashx之後可以刪除這段 == end
    const body = getters.appBody({
      "act": "login",
      "memberphone": state.member.code,
      "memberPwd": state.member.pwd,
    })

    const tokenData = await fetchData({url, body})
    
    if (tokenData.error) return showAlert(tokenData.msg)

    let tokenKey = ''
    if (tokenData && tokenData.length>0) {
      tokenKey = tokenData[0].Tokenkey  
    }
    
    return tokenKey
  },

  // (卡卡) 會員登入
  async memberLogin({state, commit, getters}, formData) {
    setLoading(true)
    const {isFrom, EnterpriseID} = state.baseInfo
    let url = 'https://' + baseUrl + '/Public/AppDataVIPOneEntry.ashx'  //才有RtAPIUrl
    // let url = 'https://' + baseUrl + '/Public/AppDataVIP.ashx'  //無RtAPIUrl
    const pwd = md5(formData.password)
    const body = {
      isFrom, EnterpriseID,
      "act": "login",
      //"memberphone": formData.account,
      "Account": formData.account,
      "memberPwd": pwd,
      "FunctionID":"450302",
    }
    
    const data = await fetchData({url, body})
    commit('setLoading', false)
    if (data.error) return showAlert(data.msg)
    commit('setLoginInfoRtAPIUrl', data[0])//為了區別是在正式 or 測試登入,取token (API service不同)
    const isRt = state.userData.RtAPIUrl !== ''// && /web/.test(location.host); //測試帳號在正式站時
    const isTestApi =  /8012test/.test((state.userData.RtAPIUrl || "").trim().toLowerCase());
    
    if (isRt && isTestApi){
      // === token start ===
      // https://wuhuapp.jh8.tw/public/AppBookingOneEntry.ashx
      url = getters.appTokenUrlWuhu
      commit('setLoading', true)
      const tokenData = await fetchData({url, body})
      // === token end ===

      commit('setLoading', false)
      
      if (tokenData.error) return showAlert(tokenData.msg)
      // 把Tokenkey加到原本的登入資料裡面
      data[0].Tokenkey = tokenData[0].Tokenkey    
    }
    
    if (formData.userTag){
      VueCookies.set('userTag', formData.userTag);  
      deviseFunction('SetSP', `{"spName":"userTag", "spValue":"${formData.userTag}"}`, '')
      commit('setBaseInfo', {userTag:formData.userTag})//登入類型,member 會員 / employee 員工
    }
    // === 登入成功的時候，要呼叫指定API === start
    url = 'https://customer-info-jinher.azurewebsites.net/api/Customer/User_App_login_date_update/' + formData.account
    axios.get( url )
    // === 登入成功的時候，要呼叫指定API === end

    
    // => 存入會員資訊 (cookie, store)
    commit('setLoginInfo', {data: data[0], rememberMe: formData.rememberMe, pwd, userTag:''})
    // => 存入會員資訊 (殼)
    deviseSetLoginInfo({type: 'set', data: data[0], rememberMe: true, pwd})
    //console.log("login...>>",formData.userTag);
    
    
    
    // 取完後再導向首頁
    getters.router.push('/')
  },
  // (卡卡) 會員登出
  memberLogout({commit, getters}) {
    // 清除 殼 裡的會員資料
    deviseSetLoginInfo({type: 'reset'})
    // 清除 state 裡的會員資料
    commit('setLogout')
    // 導向登入頁
    getters.router.push('/login')
  },
  // 會員修改個人資訊
  async ensureMemberUpdateProfile({getters}, data){
    let url = 'https://' + baseUrl + '/Public/AppDataVIPOneEntry.ashx'
    let apiBody = Object.assign({}, data, { 
      act: "memberupdate_wuhu", 
      // memberphone: state.member.code,
      FunctionID: "450301",
    })
    const body = getters.appBodyWuhu(apiBody)
    const update = await fetchData({url, body})

    return new Promise(resolve => resolve(update) )
  },
  
  async fetchQrcodUrl({commit,getters}, p_data) {  
    //console.log("start fetchQrcodUrl.>>>>");    
    axios.get(p_data.url)
    .then(res => {
      //console.log("res>>>>",res);
      //const obj_res = parseU(res.data)
      const is404 = res.data && res.data.ErrorCode == '404'
      if (res.status == 200 && !res.data || is404) return;
      
      if (res.data && Object.keys(res.data).length && !is404){
        
        commit('setQrcodeData', res.data)
        if (res.data.act == 'shopcart2other') getters.router.replace({ path: '/shopMine', query: { mode: 'unfinish' }});
      }      
    }).catch(() => { 
      //console.log("error.>>>>");
      const isFullUrl = /http:|https:/.test(p_data.url);
      if (isFullUrl) deviseFunction('openWeburl', p_data.url, '') 
    })
    .finally(() => {});
    
  },
  async fetchCart2other({state,getters}){
    //搬A訂單-->B訂單帳號
    if (Object.keys(state.qrcodeData).length == 0) return;
    const url = getters.appBookUrlWuhu

    const body = Object.assign({}, {"toAccount":state.member.code},state.qrcodeData )  //
    const data = await fetchData({url, body})
    
    if (data.error) console.log(data.msg)
  },
  /**
    統一用這個先將品牌資料撈好
  */
  async fetchVipAppsByCheck({dispatch, commit}, enterpriseID) {
    await dispatch('fetchVipAppsIfNoData', enterpriseID)
    const findApp = state.vipUserData.find(app => app.EnterPriseID === enterpriseID)
    commit('setCurrentAppUser', findApp)
  },
  /**
    refresh時只呼叫卡包清單和單一品牌
  */
  async fetchVipAppsIfNoData({dispatch}, enterpriseID) {
    let brandList = state.vipUserData
    // 有資料表示為正常點按,不用重撈
    if (brandList && brandList.length>0) {
      // console.log('===> brandList > 0')
      new Promise(resolve => {resolve(true)})
    // refresh  
    } else {
      // console.log('===> brandList < 0')
      await dispatch('fetchVipAppsOnlyList', enterpriseID)
    }
  },
  /**
    只呼叫卡包清單和單一品牌
  */
  async fetchVipAppsOnlyList({dispatch, getters, commit}, enterpriseID) {
    if (getters.isLogin === false) return

    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody, { 
      act: 'GetKKVipAPP',
      FunctionID: "450301", 
    })
    const apps = await fetchData({url, body})

    if (apps.error) return

    await commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    await commit('setVipUserLoaded')
    // 將"單一" app 的會員資料存入
    let app = await apps.find(curApp => {
      return curApp.EnterpriseID === enterpriseID
    })

    await dispatch('fetchVipAppData', {app: app})
  },
  // (卡卡) 取已綁定的企業 app 列表
  async fetchVipApps({state, dispatch, getters, commit}) {
    if (getters.isLogin === false) return

    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody, { 
      act: 'GetKKVipAPP',
      FunctionID: "450301",
    })
    const apps = await fetchData({url, body})

    if (apps.error) return

    // 存入已綁定的企業 app
    commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    commit('setVipUserLoaded')
    // 將各 app 的會員資料存入
    var promise_arr = [];
    commit('setLoading', true)
    apps.forEach(app => promise_arr.push(new Promise((resolve)=>{dispatch('fetchVipAppData', {app: app, fn:p_data=>{      
      resolve(p_data);
    }})})) )
    Promise.all(promise_arr).then(()=> {
      
      setTimeout(() => {
        if (document.querySelector(".pull-down-container")) document.querySelector(".pull-down-container").scrollTop = state.cardListScrollMap;
        commit('setLoading', false)
      }, 1)
      
      
    }).catch( () => {
      // 失敗訊息 (立即)
      commit('setLoading', false)
    });
  },
  // (卡卡) 取綁定企業的基本資料
  async fetchVipAppData({state, getters, commit}, payload) {
    var isFn = isFn || function(f) {
        return typeof f === 'function';
    }
    if (getters.isLogin === false) {
      if (isFn(payload.fn)) payload.fn();
      return;
    }    
    const { app } = payload
    const { EnterpriseID, isFrom, mac } = app

    // 取會員的點數與卡資料
    const url = state.api.memberUrl
    const body = {
      act: "GetVipAndCardInfo",
      Account: state.member.code,
      mac, isFrom, EnterpriseID,
      Tokenkey: state.member.Tokenkey,
    }
    const vipCardInfo = await fetchData({url, body})
    if (vipCardInfo && (typeof vipCardInfo !== 'undefined')) {
      if (vipCardInfo.error) {
        if (isFn(payload.fn)) payload.fn(vipCardInfo.error);
        return;
      }

      if (vipCardInfo.querydata_return_info 
        && (typeof vipCardInfo.querydata_return_info !== 'undefined')) {
        // 將取得的資料存入入 userData 裡面
        const ticketData = vipCardInfo.querydata_return_info.ticket_info
        const cardData   = vipCardInfo.querydata_return_info.card_info
        const vipData    = vipCardInfo.querydata_return_info.vip_info
        if (isFn(payload.fn)) payload.fn(vipCardInfo);
        // 取企業會員的卡片圖檔
        let cardImg
        if (app.Dir !== '' && app.FileName !== '') cardImg = 'https://' + baseUrl + app.Dir + app.FileName
        if (cardData.CardFacePhoto !== '') cardImg = app.apiip.substring(0, (app.apiip.indexOf('/public')+1)) + cardData.CardFacePhoto

        // 設定要存入的會員資料
        const userData = {
          Sex:           vipData.sex,
          mac:           app.mac,
          Name:          vipData.CnName,
          Email:         vipData.Email,
          isFrom:        app.isFrom,
          Address:       vipData.Addr,
          Mobile:        vipData.mobile,
          Account:       vipData.CardNO,
          Birthday:      vipData.BirthDay,
          ExpiredDate:   cardData.ExpiredDate,
          EnterPriseID:  app.EnterpriseID,
          CardID:        vipData.CardId,
          CardNO:        vipData.CardNO,
          cardGID:       cardData.GID,
          cardPoint:     parseInt(cardData.Points),
          cardTicket:    ticketData,
          CardTypeCode:  cardData.CardTypeCode,
          CardTypeName:  cardData.CardTypeName,
          CardFacePhoto: cardImg,
        }

        commit('setVipUserData', userData)
      }
    }
  },
  // 取會員資料 => 主要是點數給card/Member.vue和card/Point.vue
  async fetchVipAndCardInfo({getters, commit}, tokenKey) {
    // 必須為登入狀態 && 有 currentAppUser
    if (getters.isLogin === false || state.currentAppUser.EnterPriseID === undefined) return
  
    const url = state.api.memberUrl // getters.appMemberApiUrl
    let body = getters.appBody({act: 'GetVipAndCardInfo'})
    body.Tokenkey = tokenKey // 把token改成卡包內的
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberVipData', data)  
  },
  // 綠界付款資訊
  async fetchEcPay({ commit}, payData) {
    const url = payData.urlToken;
    const body = payData.RtnModel;
    const data = await fetchData({url, body})
    
    console.log("setEcPayData",data);
    commit('setEcPayData', data) 
  },
  // 取特殊商品下拉數據源EC_SpecLimit
  async fetchSpecLimit({getters, commit}) {
    // 必須為登入狀態 && 有 currentAppUser
    if (getters.isLogin === false) return   
    const url = state.api.memberUrl 
    let body = getters.appBodyWuhu({act: 'GetData',"DataName":"EC_SpecLimit"})    
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setSpecLimit', data)  
  },
  // 取特殊商品下拉數據源,付款狀態
  async fetchPayStatus({getters, commit}) {
    // 必須為登入狀態 && 有 currentAppUser
    if (getters.isLogin === false) return   
    const url = state.api.memberUrl 
    let body = getters.appBodyWuhu({act: 'GetData',"DataName":"P_PayStatus"})    
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setPayStatus', data)  
  },
  // 取會員資料 => 主要是點數給HomeMember.vue和HomePoint.vue
  async fetchVipAndCardInfoWuhu({getters, commit}) {
    // console.log('fetchVipAndCardInfoWuhu ===> start')
    // 必須為登入狀態 && 有 currentAppUser
    if (getters.isLogin === false || state.baseInfo.EnterpriseID === undefined) return
    commit('setIsVipDataLoad', false)
    const url = state.api.memberUrl // state.api.picUrl + '/public/AppVIP.ashx' //getters.appMemberApiUrl
    const body = getters.appBodyWuhu({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setIsVipDataLoad', true)
    commit('setMemberVipDataWuhu', data)  
  },
  //特約商清單
  async fetchSpecShopWuhu({getters, commit}) {
    if (getters.isLogin === false || state.baseInfo.EnterpriseID === undefined) return
    const url = state.api.memberUrl; 
    const body = getters.appBodyWuhu({act: 'getSpecShop',"FunctionID":"950201"})    
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setSpecShopWuhu', data)  
  },
  //退款/退貨原因表 EC_RefundReason 
  async fetchReasonWuhu({state, getters, commit}) {
    if (getters.isLogin === false || state.baseInfo.EnterpriseID === undefined) return
    if (Array.isArray(state.baseInfo.memberData.appReason) && state.baseInfo.memberData.appReason.length > 0) return //撈過跳過
    const url = state.api.memberUrl;
    const body = getters.appBodyWuhu({act: 'getAppReason',"FunctionID":"950201"})    
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setReasonWuhu', data)  
  },
  //訂閱制清單表 EC_Subscription 
  async fetchSubscription({state, getters, commit}) {
    if (getters.isLogin === false || state.baseInfo.EnterpriseID === undefined) return
    //if (Array.isArray(state.baseInfo.memberData.subscriptionList) && state.baseInfo.memberData.subscriptionList.length > 0) return //撈過跳過
    const url = state.api.memberUrl;
    const body = getters.appBodyWuhu({act: '1001d10_getSubscription',"FunctionID":"1001d10"})    
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setSubscription', data)  
  },
  // (卡卡) 取 currentAppUser 的相關資料
  async fetchAppUserData({dispatch, getters, commit, state}){
    // 必須為登入狀態 && 有 currentAppUser
    if (getters.isLogin === false || state.currentAppUser.EnterPriseID === undefined) return

    const url = getters.appMemberApiUrl
    const body = getters.appBody({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      commit('setMemberVipData', data)
      // 取會員的消費紀錄
      dispatch('fetchMemberPurchaseRecordData')
      // 取會員的積點交易紀錄
      dispatch('fetchMemberExpiredPointsData')
      // 取優惠卷列表資訊
      dispatch('fetchMemberTicketListData')
      // 取會員已轉贈的優惠卷資訊
      dispatch('fetchMemberGaveTicketListData')
      // 取尚未能使用的優惠卷資料
      dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  // (卡卡) 取 currentAppUser 的相關資料 - 五戶
  async fetchAppUserDataWuhu({getters, commit, state}){
    // 必須為登入狀態 && 有 currentAppUser
    if (getters.isLogin === false || state.baseInfo.EnterpriseID === undefined) return

    const url = state.api.memberUrl // state.api.picUrl + '/public/AppVIP.ashx' //getters.appMemberApiUrl
    const body = getters.appBodyWuhu({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      commit('setMemberVipData', data)
      // // 取會員的消費紀錄
      // dispatch('fetchMemberPurchaseRecordData')
      // // 取會員的積點交易紀錄
      // dispatch('fetchMemberExpiredPointsData')
      // // 取優惠卷列表資訊
      // dispatch('fetchMemberTicketListData')
      // // 取會員已轉贈的優惠卷資訊
      // dispatch('fetchMemberGaveTicketListData')
      // // 取尚未能使用的優惠卷資料
      // dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  // (卡卡) 取 cardQRcode
  async fetchCardQRCodeData({state, commit, getters, dispatch}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )

    const url = state.api.memberUrl
    let body = getters.appBody({act: 'GetVipCardQrCodeStr', GID})

    body.Tokenkey = await dispatch('childLogin') // 把token改成卡包內的
    
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },
  // (龍海) 取 cardQRcode
  async fetchWuhuCardQRCodeData({state, commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )

  },
  // (卡卡) 進入企業 app
  async AccessInApp({commit}, userData) {
    // 存入企業號 (用於線上點餐，回到 卡+ 時正確導向到指定頁面)
    deviseFunction('SetSP', `{"spName":"currentEnterPriseID", "spValue":"${userData.EnterPriseID}"}`, '')
    VueCookies.set('currentEnterPriseID', userData.EnterPriseID)
    // 企業 app 點擊紀錄 +1
    commit('setAppClicks', userData.EnterPriseID)
    // 先存入 currentAppUser
    await commit('setCurrentAppUser', userData)
  },
  /* 企業公開資料相關 */

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataWuhu({dispatch}){
    // console.log('fetchAppPublicDataWuhu ===> start')

    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')

    // 取首頁主 banners
    dispatch('fetchIndexSlideBannersWuhu')

    // 取首頁副 banners
    dispatch('fetchIndexSubBannerWuhu')

    // // 取最新消息的資料
    // dispatch('fetchNewsDataWuhu')

    // 取最新消息的分類
    // dispatch('fetchNewsTypes')
    // 取品牌資訊的資料
    // dispatch('fetchBrandInfo')
    // 取門店資訊的資料
    // dispatch('fetchStoreListDataWuhu')
    // 取 menu 資料
    // dispatch('fetchMenuData')

  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataStoreWuhu({dispatch}){
    // console.log('fetchAppPublicDataStoreWuhu ===> start')

    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')

    // 取門店資訊的資料
    dispatch('fetchStoreListDataWuhu')
    
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataNewsWuhu({dispatch}){
    // console.log('fetchAppPublicDataNewsWuhu ===> start')

    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')

    // 取最新消息的資料
    dispatch('fetchNewsDataWuhu')
    
  },

  // (公開) 取 currentApp 的公開資料 - HomeMember頁
  async fetchAppPublicDataMemberWuhu({dispatch}){
    // console.log('fetchAppPublicDataMemberWuhu ===> start')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')
    // 取點數等資料
    dispatch('fetchVipAndCardInfoWuhu')
  },

  // (公開) 取 currentApp 的公開資料 - HomePoint頁
  async fetchAppPublicDataPointWuhu({dispatch}){
    // console.log('fetchAppPublicDataPointWuhu ===> start')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')
    // 取點數等資料
    dispatch('fetchVipAndCardInfoWuhu')
    // 取會員的積點交易紀錄
    dispatch('fetchMemberExpiredPointsDataWuhu')
  },

  // (公開) 取 currentApp 的公開資料 - HomeCoupon頁
  async fetchAppPublicDataCouponWuhu({dispatch}){
    // console.log('fetchAppPublicDataCouponWuhu ===> start')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')
    // 取優惠卷列表資訊
    dispatch('fetchMemberTicketListDataWuhu')
    // 取會員已轉贈的優惠卷資訊
    dispatch('fetchMemberGaveTicketListDataWuhu')
    // 取尚未能使用的優惠卷資料
    dispatch('fetchMemberNotAvailableTicketsWuhu')
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataDetailWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')
    // 取首頁主 banners
    dispatch('fetchIndexSlideBanners')
    // 取首頁副 banners
    dispatch('fetchIndexSubBanner')
    // 取最新消息的資料
    // dispatch('fetchNewsData')
    // 取最新消息的分類
    // dispatch('fetchNewsTypes')
    // 取品牌資訊的資料
    // dispatch('fetchBrandInfo')
    // 取門店資訊的資料
    // dispatch('fetchStoreListData')
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataDetailStoreWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')
    // 取門店資訊的資料
    dispatch('fetchStoreListDataDetail')
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataDetailNewsWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')
    // 取門店資訊的資料
    dispatch('fetchNewsData')
  },

  // (公開) 取 currentApp 的公開資料 - card/Member頁
  async fetchAppPublicDataDetailMemberWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')

    // 卡包內登入
    let tokenKey = await dispatch('childLogin')

    // 取點數等資料
    dispatch('fetchVipAndCardInfo', tokenKey)
  },

  // (公開) 取 currentApp 的公開資料 - card/Point
  async fetchAppPublicDataDetailPointWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')

    // 卡包內登入
    let tokenKey = await dispatch('childLogin')

    // 取點數等資料
    dispatch('fetchVipAndCardInfo', tokenKey)
    // 取會員的積點交易紀錄
    dispatch('fetchMemberExpiredPointsData', tokenKey)
  },

  // (公開) 取 currentApp 的公開資料 - card/Coupon
  async fetchAppPublicDataDetailCouponWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')

    // 卡包內登入
    let tokenKey = await dispatch('childLogin')

    // 取優惠卷列表資訊
    dispatch('fetchMemberTicketListData', tokenKey)
    // 取會員已轉贈的優惠卷資訊
    dispatch('fetchMemberGaveTicketListData', tokenKey)
    // 取尚未能使用的優惠卷資料
    dispatch('fetchMemberNotAvailableTickets', tokenKey)
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicData({dispatch}){
    // 取企業 app 的 connect
    await dispatch('fetchAppConnect')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogo')
    // 取首頁主 banners
    dispatch('fetchIndexSlideBanners')
    // 取首頁副 banners
    dispatch('fetchIndexSubBanner')
    // 取最新消息的資料
    dispatch('fetchNewsData')
    // 取最新消息的分類
    dispatch('fetchNewsTypes')
    // 取品牌資訊的資料
    dispatch('fetchBrandInfo')
    // 取門店資訊的資料
    dispatch('fetchStoreListData')
    // 取 menu 資料
    dispatch('fetchMenuData')
  },
  // (公開) 取企業 App 的 db connect 資料
  async fetchAppConnect({state, commit}){
    const url = state.api.memberUrl
    const body = {"act":"db_name","EnterPriseID": state.currentAppUser.EnterPriseID}
    const connect = await fetchData({url, body})
    if (connect.error) return

    commit('setAppConnect', connect)
  },
  // (公開) 取企業 App 的 LOGO 資料
  async fetchAppLogo({state, getters, commit}){
    const url = state.api.memberUrl
    const body = {act: "getapplogo", EnterPriseID: getters.currentApp.EnterpriseID}
    const data = await fetchData({url, body})
    if (data.error) return

    const logoUrl = getters.srcUrl + data[0].Dir + data[0].FileName

    commit('setAppLogo', logoUrl)
  },

  // (公開) 取企業 App 的 LOGO 資料
  async fetchAppLogoDetail({state, getters, commit}){
    const url = state.api.publicUrl
    const body = {act: "getapplogo", EnterPriseID: state.currentAppUser.EnterPriseID}
    const data = await fetchData({url, body})
    if (data.error) return

    const logoUrl = getters.srcUrl + data[0].Dir + data[0].FileName

    commit('setAppLogo', logoUrl)
  },

  // [五戶](公開) 取企業 App 的 LOGO 資料
  async fetchAppLogoWuhu({state, commit}){
    const url = state.api.noTokenUrl
    const body = {act: "getapplogo", EnterPriseID: state.baseInfo.EnterpriseID}
    const data = await fetchData({url, body})

    if (data.error || !Array.isArray(data) || data.length == 0) return

    const logoUrl = state.api.picUrl + data[0].Dir + data[0].FileName    
    //jis_240X50.jpg
    commit('setAppLogoWuhu', logoUrl)
  },

  // [五戶]撈[我的預約]清單
  async fetchMyBookList({getters, commit}){
    const url = getters.appBookUrlWuhu
    const body = getters.appBodyWuhu({act: 'get_mybooking'})
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setMyBookList', data.mybooking)
    commit('setMyBookStatusList', data.status)
  },
  // [五戶]撈[商城]大小類等
  async fetchMallData({state}){
    var isFn = isFn || function(f) {
        return typeof f === 'function';
    }
    
    if (isFn(state.mallData.reloadShopFn)) state.mallData.reloadShopFn();
    
  },
  // [五戶]撈[商城]記錄
  async fetchShopMine({state}){
    var isFn = isFn || function(f) {
        return typeof f === 'function';
    }
    
    if (isFn(state.mallData.reloadShopMineFn)) state.mallData.reloadShopMineFn();
    
  },
  // [五戶]撈[商城]小類下品項,或品項的規格
  async fetchMallKindData({state},query){
    var isFn = isFn || function(f) {
        return typeof f === 'function';
    }
    
    let isMallKindLoad = (query.goto!=undefined)//商城只允許第一個小類可刷新
    let isMallProductLoad = (query.type!=undefined && query.type === 'product');
    
    if (isMallProductLoad && isFn(state.mallData.reloadProductFn)){
      state.mallData.reloadProductFn();
      
    }else if (isMallKindLoad && isFn(state.mallData.reloadGoKindFn)) {
      state.mallData.reloadGoKindFn();
    } 
  },
  // (公開) 取主畫面可滑動的 Banners 資料
  async fetchIndexSlideBanners({state, getters, commit}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandBanner",BannerType:"M"})
    const data = await fetchData({url, body})
    state.publicData.index.slideBannerIsLoaded = true
    if (data.error) return
    
    commit('setIndexSlideBanners', data)
  },
  // (公開) 取主畫面可滑動的 Banners 資料,主企業,
  // JT開頭,捷愛送
  async fetchIndexSlideBannersWuhu({state, getters, commit}){
    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrandBanner",BannerType:"M"})
    let data = await fetchData({url, body})
    state.baseInfo.publicData.index.slideBannerIsLoaded = true
    if (data.error) return
    if (state.baseInfo.userTag == 'employee' && Array.isArray(data) && data.length > 0) {
      const jt_list = data.filter(jt=>{        
        return jt.BannerID.trim().toLowerCase().indexOf('jt') == 0
      });
      
      const jt= {"GID":"a46f2ad3-968c-4de7-8672-cb00e4ca2944","EnterpriseID":"53239538","No":99,"BannerID":"JT000001","BannerName":"捷愛送","LinkType":"InnerURL","LinkPostion":"shop","SaleTime":"2020-08-25 00:00:00","CloseTime":"2020-08-25 00:00:00","PicURL":"https:\\/\\/staging.jh8.tw\\/upload\\/53239538\\/E_Banner\\/a46f2ad3-968c-4de7-8672-cb00e4ca2944.jpg?v20200825145435","BannerType":"$M$","OuterURL":"","LastModify":"2020-08-25 14:51:03","LastOP":"53239538","Remark":"","TypeName":""};
      if (jt_list.length == 0) jt_list.push(jt);      
      commit('setIndexSlideBannersWuhu', jt_list);
    }else{
      commit('setIndexSlideBannersWuhu', data)
    }
    
  },
  // (公開) 取固定的副 Banner 資料
  async fetchIndexSubBanner({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandBanner",BannerType:"D"})
    const data = await fetchData({url, body})
    state.publicData.index.subBannerIsLoaded = true
    if (data.error) return

    commit('setIndexSubBanner', data)
  },
  // (公開) 取固定的副 Banner 資料,// JT開頭,捷愛送
  async fetchIndexSubBannerWuhu({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrandBanner",BannerType:"D"})

    const data = await fetchData({url, body})
    state.baseInfo.publicData.index.subBannerIsLoaded = true
    if (data.error) return
    if (state.baseInfo.userTag == 'employee' && Array.isArray(data) && data.length > 0) {
      const jt_list = data.filter(jt=>{
        return jt.BannerID.trim().toLowerCase().indexOf('jt') == 0
      });
      const jtSub= {"GID":"4711399d-84e8-42d3-baaa-1e343b4cd848","EnterpriseID":"53239538","No":99,"BannerID":"JT2001","BannerName":"次橫幅","LinkType":"InnerURL","LinkPostion":"shopMine","SaleTime":"2020-08-25 00:00:00","CloseTime":"2020-08-25 00:00:00","PicURL":"https:\\/\\/staging.jh8.tw\\/upload\\/53239538\\/E_Banner\\/4711399d-84e8-42d3-baaa-1e343b4cd848.jpg?v20200825145946","BannerType":"$D$","OuterURL":"","LastModify":"2020-08-25 14:59:18","LastOP":"53239538","Remark":"","TypeName":""};
      if (jt_list.length == 0) jt_list.push(jtSub);
      //console.log("data fetchIndexSubBannerWuhu",jt_list);
      commit('setIndexSubBannerWuhu', jt_list);
    }else{
      commit('setIndexSubBannerWuhu', data);
    }
    
    
    
    
  },
  // (公開) 取最新消息的資料
  async fetchNewsData({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsData', data)
  },
  // (公開) 取最新消息的資料 - 五互
  async fetchNewsDataWuhu({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsDataWuhu', data)
  },
  // (公開) 取最新消息的分類
  async fetchNewsTypes({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNewsType"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsTypes', data)
  },
  // (公開) 取品牌資訊的資料
  async fetchBrandInfo({state, commit, getters}){
    const url = state.api.kakarUrl
    const body = { act: "getbrandmsg", EnterpriseID: getters.currentApp.EnterpriseID, brandid: "1,2,3,4,5,6,7,8,9" }
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setBrandInfo', data)
  },
  // (公開) 取門店資訊的資料
  async fetchStoreListData({state, getters, commit}){
    // 取得主企業號 ID & connect
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.currentAppUser.EnterPriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 取得門店資料
    const urlB = state.api.publicUrl
    const bodyB = getters.appBody({act:"GetStoreList"})
    const stores = await fetchData({url: urlB, body: bodyB})

    // 取得門店的線上點餐參數
    const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode"}

    // 將線上點餐的參數(.Mode)存入到門店資料裡面
    const storeMode = await fetchData({url: memberUrl, body: bodyC})
    stores.forEach(store => {
      const mode = storeMode.find(m => m.ShopID === store.OrgCode)
      store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
      store.ShopID = (mode) ? mode.ShopID : undefined
    })

    // 存入到 Vuex 裡面
    commit('setStoreListData', {stores, MainEnterPriseID, MainConnect})
  },

  // (公開) 取門店資訊的資料
  async fetchStoreListDataDetail({state, getters, commit}){
    // console.log('fetchStoreListDataDetail ===> start')
    // 取得主企業號 ID & connect
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.currentAppUser.EnterPriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 取得門店資料
    const urlB = state.api.publicUrl
    const bodyB = getters.appBody({act:"GetStoreList"})
    const stores = await fetchData({url: urlB, body: bodyB})

    // 取得門店的線上點餐參數
    const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode"}

    // 將線上點餐的參數(.Mode)存入到門店資料裡面
    const storeMode = await fetchData({url: memberUrl, body: bodyC})
    stores.forEach(store => {
      const mode = storeMode.find(m => m.ShopID === store.OrgCode)
      store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
      store.ShopID = (mode) ? mode.ShopID : undefined
    })

    // 存入到 Vuex 裡面
    commit('setStoreListData', {stores, MainEnterPriseID, MainConnect})
  },

  // (公開) 取門店資訊的資料 - 五互
  async fetchStoreListDataWuhu({state, getters, commit}){
    // 取得主企業號 ID & connect
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.baseInfo.EnterpriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 取得門店資料
    const urlB = state.api.publicUrl
    const bodyB = getters.appBodyWuhu({act:"GetStoreList"})
    const stores = await fetchData({url: urlB, body: bodyB})

    // 取得門店的線上點餐參數
    const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode"}

    // 將線上點餐的參數(.Mode)存入到門店資料裡面
    const storeMode = await fetchData({url: memberUrl, body: bodyC})
    if (!Array.isArray(stores)) return;
    stores.forEach(store => {
      const mode = storeMode.find(m => m.ShopID === store.OrgCode)
      store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
      store.ShopID = (mode) ? mode.ShopID : undefined
    })

    // 存入到 Vuex 裡面
    commit('setStoreListDataWuhu', {stores, MainEnterPriseID, MainConnect})
  },
  // (公開) 取門店資訊的資料 - 五互
  async fetchStoreListDataFtype({state, getters, commit}){
    // 取得主企業號 ID & connect
    const ftype = state.baseInfo.publicData.storeFtype || [];
    if (Array.isArray(ftype) && ftype.length ) return; 
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.baseInfo.EnterpriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 取得門店資料
    const urlB = state.api.publicUrl
    const bodyB = getters.appBodyWuhu({act:"GetStoreList","Ftype":"true"})
    const stores = await fetchData({url: urlB, body: bodyB})

    // 取得門店的線上點餐參數
    const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode"}

    // 將線上點餐的參數(.Mode)存入到門店資料裡面
    const storeMode = await fetchData({url: memberUrl, body: bodyC})
    if (!Array.isArray(stores)) return;
    stores.forEach(store => {
      const mode = storeMode.find(m => m.ShopID === store.OrgCode)
      store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
      store.ShopID = (mode) ? mode.ShopID : undefined
    })
    
    // 存入到 Vuex 裡面
    commit('setStoreListDataFtype', {stores, MainEnterPriseID, MainConnect})
  },
  // (公開) 取 Menu 資料
  async fetchMenuData({state, getters, dispatch}) {
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType"})
    const menuKindIds = await fetchData({url, body})
    if (menuKindIds.error) return
    menuKindIds.forEach(item => dispatch('fetchMenuMainTypeData', {code: item.KindID, name: ''}) )
  },
  // (公開) 取 Menu 大類的資料
  async fetchMenuMainTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.mainType.find(item => { return item.code === payload.code }) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType", MKindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuMainTypeCodeName', payload)
    commit('setMenuMainTypeListData', { code: payload.code, data: data })
  },
  // (公開) 取 Menu 小類的資料
  async fetchMenuSubTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.subType.find(item => item.code === payload.code ) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProduct", KindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuSubTypeCodeName', payload)
    commit('setMenuSubTypeListData', { code: payload.code, data: data } )
  },
  fetchFoodmarketMulti({state,dispatch}){
    // console.log("fetchFoodmarketMulti",state.mallData.foodkind);
    //迴圈一起跑會GG (API無法負荷??),已改為1筆1筆取,第一筆回應,再next    
    if (Array.isArray(state.mallData.foodkind) && state.mallData.foodkind.length > 0) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[0]["ID"],kIndex:0})
  },
  // 取全部小類下商品
  async fetchFoodmarketData({commit,getters,dispatch},p_obj) {
    const url =  getters.appBookUrlWuhu;    
    // console.log("fetchFoodmarketData",url,p_obj.kindID,p_obj.kIndex);    
    const body = getters.appBodyWuhu({"act": 'get_foodmarket',"FoodKind": p_obj.kindID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0"; 
    
    if (isErr || data.error) return
    commit('setFoodmarketData', {foods: (data.FoodMarket || []), kindID: p_obj.kindID})
    const p_index = p_obj.kIndex+1;
    //已改為1筆1筆取,等待第一筆回應,再next
    if (state.mallData.foodkind.length > p_index) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[p_index]["ID"],kIndex:p_index})
      
  },
  /* 企業會員資料相關 */

  // 取會員的消費紀錄
  async fetchMemberPurchaseRecordData({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBody({"act":"GetVipPosItems"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberPurchaseRecordData', data)
  },
  // 取會員的積點交易紀錄
  async fetchMemberExpiredPointsData({state, commit, getters}, tokenKey) {
    const url = state.api.memberUrl
    let body = getters.appBody({"act":"GetVipRecord", "TradeTypeCode":"4,-4,6,-6,7,-7,15,-15,16,-16,17,-17"})
    body.Tokenkey = tokenKey // 把token改成卡包內的
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberExpiredPointsData', data)
  },
  // 取會員的積點交易紀錄 - 五互
  async fetchMemberExpiredPointsDataWuhu({state, commit, getters}) {
    const url = state.api.memberUrl
    // 4,   -4: 積分兌換, 積分兌換退回
    // 7,   -7: 積分換券, 積分換券退回
    // 15, -15: 公關贈點, 公關贈點退回
    // 16, -16: 活動贈點, 活動贈點退回
    // 17, -17: 商品贈點, 商品贈點退回 ==文字改為==> 滿額贈點、滿額贈點退回
    // 23, -23: 首購禮,   首購禮退回
    const body = getters.appBodyWuhu({"act":"GetVipRecord", "erow":30,"TradeTypeCode":"4,-4,7,-7,15,-15,16,-16,17,-17,23,-23,31,-31,25,-25"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberExpiredPointsDataWuhu', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  // 秋林版 - 目前已改用黃大哥版(如下)
  async fetchMemberConsumerPointsDataWuhu_bak({state, commit, getters}) {
    const url = state.api.consumerUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_Points_Info_List_all", 
      "FunctionID":"950201",
      "orderby":"end_day desc, deed_no desc",
      "srow": 0 // srow=0代表傳全部
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDataWuhu({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu

    const dynamicBody = (payload || {})
    let fixData = {
      "act":"get_deedpoints",
      "orderby":"end_day desc, deed_no desc"
    }
    let allData = Object.assign({}, fixData, dynamicBody)

    const body = getters.appBodyWuhu(allData)
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的單一筆[契約][消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDetailWuhu({state, commit, getters}, payload) {
    const url = state.api.consumerUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_PointsBalance_Trade", 
      "FunctionID":"950201",
      "orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "deed_no": btoa(payload.deed_no)
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointDetailWuhu', data)
  },
  //取得已完成訂單
  async fetchOrder_single({commit, getters}, payload) {
    commit('setSingleFinish', [])
    const url = state.api.consumerUrl
    const body = getters.appBodyWuhuEncode({
      "act":"get_myorder_qry", 
      "FunctionID":"950201",  
      //"orderby":"LastModify",    
      "srow": 0 // srow=0代表傳全部
    }, {
      "OrderID": btoa(payload.ID)
    })
    const data = await fetchData({url, body})
   
    if (data.error) return

    commit('setSingleFinish', data)
  },
  // 取會員的單一筆[訂單][消費點數紀錄] - 用Order No
  async fetchOneOrderByOrderNo({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu

    if (payload) {
      const dynamicBody = (payload.requestBody || {})
      let fixData = {
        "act":"get_orderitems"
      }
      let allData = Object.assign({}, fixData, dynamicBody)

      const body = getters.appBodyWuhu(allData)
      const data = await fetchData({url, body})
      if (data.error) return

      let orderDetail = data?data.OrderItem:null

      commit('setOneOrderDetail', {
        orderDetail: orderDetail, 
        arrayIndex: payload.arrayIndex
      }) 
    }
    
  },
  async ensureGetDeviseGPS({/*state,*/ commit}) {
    //
    if (typeof(JSInterface) !== 'undefined') {
      // (在殼裡面) call 跟殼取 GPS 資訊的 API 與 callback function
      deviseFunction('gps', '', '"cbFnSyncDeviseGPS"')
    } else {
      // 不在殼裡面
      if ("geolocation" in navigator) {
        /* geolocation is available */
        navigator.geolocation.getCurrentPosition((position)=>{
          console.log('navigator ===> position =>', position)
          // 轉成 google maps api 需要的預設格式
          const gpsData = {gps: {
            lat: position.coords.latitude, 
            lng: position.coords.longitude} }
          // 將 gps 存入 state.baseInfo
          commit('setBaseInfo', gpsData)
        }, ()=>{
          console.log('navigator ===> error => no navigator')
        })
      } else {
        /* geolocation IS NOT available */
      }
    }
  },
  // 取可領用的優惠卷列表資訊 + 領取優惠券 - 五互
  async fetchMemberDrawTicketListDataWuhu({state, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetDrawTicketList" })
    const data = await fetchData({url, body})
    if (data.error) return
  },
  // 取優惠卷列表資訊
  async fetchMemberTicketListData({state, commit, getters}, tokenKey){
    const url = state.api.memberUrl
    let body = getters.appBody({ act: "GetVipTicket" })
    body.Tokenkey = tokenKey // 把token改成卡包內的
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberTicketListData', data)
  },
  // 取優惠卷列表資訊 - 五互
  async fetchMemberTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetVipTicket" })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberTicketListDataWuhu', data)
  },
  // 取已贈送的優惠卷列表資訊
  async fetchMemberGaveTicketListData({state, commit, getters}, tokenKey){
    const url = state.api.memberUrl
    let body = getters.appBody({ act: "GetGaveTicketRecord" })
    body.Tokenkey = tokenKey // 把token改成卡包內的
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberGaveTicketListData', data)
  },
  // 取已贈送的優惠卷列表資訊 - 五互
  async fetchMemberGaveTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetGaveTicketRecord" })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberGaveTicketListDataWuhu', data)
  },
  // 取尚未能使用的優惠卷資料
  async fetchMemberNotAvailableTickets({state, commit, getters}, tokenKey){
    const url = state.api.memberUrl
    let body = getters.appBody({ act: "NotTodayTicketList" })
    body.Tokenkey = tokenKey // 把token改成卡包內的
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberNotAvailableTickets', data)
  },
  // 取尚未能使用的優惠卷資料 - 五互
  async fetchMemberNotAvailableTicketsWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "NotTodayTicketList" })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberNotAvailableTicketsWuhu', data)
  },
  // 優惠卷贈送
  async ensureMemberTransTicket({state, getters}, payload){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu(Object.assign({}, { act: "SetTicketToOther" }, payload ))
    const data = await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // 取優惠卷明細的使用條款與兌換條件
  fetchMemberTicketInfoData({dispatch}, ticket){
    // 取該卷使用的 QRcode
    dispatch('fetchTicketQRCodeData', ticket.GID)
    // 取該卷的條款與規則
    dispatch('fetchTicketTradeRules', ticket.TicketTypeCode)
  },
  // 取優惠卷條碼 QRcode
  async fetchTicketQRCodeData({state, commit, getters}, ticketGID){
    // 先清空現在的 QRcode 資料
    commit('setTicketQRCodeData', {gid: '', image: ''} )
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetTicketQrCodeStr", GID: ticketGID, QRCodeScale: 4 })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setTicketQRCodeData', {gid: data.QrCode, image: data.QrCodeImg} )
  },
  // 取優惠卷明細資訊: 規則條款
  async fetchTicketTradeRules({state, commit, getters, dispatch}, TypeCode){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRules", TicketTypeCode: TypeCode })
    let data = await fetchData({url, body})
    if (data.error) return

    // data = [{
    //   "OrderMinMoney":"",
    //   "IsFixedPoint":"",
    //   "LastOP":"huaichen",
    //   "EndDate":"2020/3/31 上午 12:00:00",
    //   "OrderMaxMoney":"",
    //   "BeginDate2":"2020-03-09",
    //   "TradeDiscount2":"",
    //   "BeginTime":"",
    //   "PresentPoint":"",
    //   "BrithTimes":"",
    //   "Priority":"0",
    //   "Week7":"True",
    //   "TradeRuleName":"(獨家優惠)金子半之助香香雞天丼優惠券 規則",
    //   "EnterPriseID":"24435384",
    //   "Remark":"1.憑本畫面至全台金子半之助內用點香香雞天丼，送氣泡飲一杯優惠。\n2.氣泡飲不得要求轉換、轉讓為現金及其他商品，亦不得轉售。\n3.本活動不適用於外帶服務，請於點餐前出示本畫面。\n4.活動到期自動失效，請於指定日期前兌換完畢，逾期恕無法使用。\n5.本活動如有未盡事宜，本公司保留修改、終止、變更活動內容細節之權利，且不另行通知。",
    //   "MaxUseCount":"",
    //   "Week1":"True",
    //   "Week2":"True",
    //   "Week3":"True",
    //   "Week4":"True",
    //   "Week5":"True",
    //   "Week6":"True",
    //   "CardTypeCode":"CR20030901",
    //   "TradeAmount":"",
    //   "FixedPoint":"",
    //   "GID":"6603d37c-39b9-4218-8da0-f6ae67b0e2db",
    //   "Discount":"",
    //   "BirthDiscount":"",
    //   "LastModify":"2020/3/5 下午 06:10:06",
    //   "EndTime":"",
    //   "UseCountIsShop":"True",
    //   "TradeDiscount":"",
    //   "Many_times":"",
    //   "BirthDayRuleType":"0",
    //   "IsUseDate":"",
    //   "EndDate2":"2020-03-31",
    //   "PresenBonus":"",
    //   "TradeRuleCode":"RU20030901",
    //   "VipMinPoint":"0.00",
    //   "IsTimes":"",
    //   "If_Birthday_Discount":"",
    //   "BeginDate":"2020/3/9 上午 12:00:00",
    //   "TradeTypeCode":"T2",
    //   "ConfirmUseCount":""  
    // }]

    // 取優惠卷的兌換條件: 商品
    dispatch('fetchTicketUsageRuleFoods', data[0].TradeRuleCode)
    // 取優惠卷的兌換條件: 門店
    dispatch('fetchTicketUsageRuleShops', data[0].TradeRuleCode)
    // 設定使用條款與適用日
    commit('setMemberTicketInfoData', { ruleData: data[0], currentTypeCode: TypeCode })
  },
  // 取優惠卷明細資訊的兌換條件: 商品
  async fetchTicketUsageRuleFoods({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRuleFoods", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    if (data.error) return

    // data = [
    //   {
    //     "TradeRuleCode":"RU20020502",
    //     "EnterPriseID":"24435384",
    //     "Remark":"",
    //     "GID":"521723c3-d89f-4f11-9dfe-2636e9e392fa",
    //     "FoodName":"(APP)(海苔)墨魚甜不辣八折",
    //     "LastModify":"2020/2/5 上午 11:02:35",
    //     "IsSaleFood":"True",
    //     "FoodID":"9A04999007",
    //     "IsFoodKind":"True",
    //     "LastOP":"maoleon"
    //   },
    //   {
    //     "TradeRuleCode":"RU20020502",
    //     "EnterPriseID":"24435384",
    //     "Remark":"",
    //     "GID":"2e109d75-c6f7-4bc7-b471-23b02cd7f6ed",
    //     "FoodName":"(APP)(塔塔)墨魚甜不辣八折",
    //     "LastModify":"2020/2/5 上午 11:02:35",
    //     "IsSaleFood":"True",
    //     "FoodID":"9A04999008",
    //     "IsFoodKind":"True",
    //     "LastOP":"maoleon"
    //   }
    // ]

    commit('setMemberTicketInfoData', { usageFoods: data, currentRuleCode: RuleCode } )
  },

  // 取優惠卷明細資訊的兌換條件: 門店
  async fetchTicketUsageRuleShops({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GenerateTicketTradeRuleShops", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    if (data.error) return

    // only for test
    // data = {"ErrorCode":"0","ErrorMsg":"","Remark":'[{"TradeRuleCode":"RU20020502","EnterPriseID":"24435384","GID":"860a5c88-9de8-42d3-8ae2-9f62c1252596","ShopName":"新莊幸福店","LastModify":"2020/2/7 下午 11:46:19","ShopID":"0003","LastOP":"maoleon"},{"TradeRuleCode":"RU20020502","EnterPriseID":"24435384","GID":"6c65ad6e-6a5c-4039-8084-5d02a9579b4f","ShopName":"淡水店","LastModify":"2020/2/7 下午 11:46:19","ShopID":"0004","LastOP":"maoleon"}]'}

    // only for test
    // data = [
    //   {
    //     "TradeRuleCode":"RU20020502",
    //     "EnterPriseID":"24435384",
    //     "GID":"860a5c88-9de8-42d3-8ae2-9f62c1252596",
    //     "ShopName":"新莊幸福店",
    //     "LastModify":"2020/2/7 下午 11:46:19",
    //     "ShopID":"0003",
    //     "LastOP":"maoleon"
    //   },
    //   {
    //     "TradeRuleCode":"RU20020502",
    //     "EnterPriseID":"24435384",
    //     "GID":"6c65ad6e-6a5c-4039-8084-5d02a9579b4f",
    //     "ShopName":"淡水店",
    //     "LastModify":"2020/2/7 下午 11:46:19",
    //     "ShopID":"0004",
    //     "LastOP":"maoleon"
    //   }
    // ]

    commit('setMemberTicketInfoData', { usageShops: data, currentRuleCode: RuleCode })
  },
}

export default new Vuex.Store({ state, mutations, getters, actions })
