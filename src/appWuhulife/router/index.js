// vue basic lib
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// vue router 設定(桌面版)
const routes = [
  // === 龍海主頁面 ===
  { path: '/',  meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeIndex.vue') },
  { path: '/member',  meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeMember.vue') },
  { path: '/news',  meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeNews.vue') },
  { path: '/newspage/:id',  meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeNewspage.vue') },
  { path: '/store', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeStore.vue') },
  { path: '/openUrl/:pageName', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/openUrl.vue') },
  { path: '/point', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomePoint.vue') },
  { path: '/consumePoint', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeConsumePoint.vue') },
  { path: '/coupon', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeCoupon.vue') },
  { path: '/booking', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeBooking.vue') },
  { path: '/bookingClean', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeBookingClean.vue') },
  { path: '/bookingHotel', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeBookingHotel.vue') },
  { path: '/bookingMine', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeBookingMine.vue') },
  { path: '/shop', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeShop.vue') },
  { path: '/shopType', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeShopType.vue') },
  { path: '/shopCart', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeShopCart.vue') },
  { path: '/shopMine', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeShopMine.vue') },
  { path: '/contactUs', meta: { layout: 'Home' }, component: () => import('src/appWuhulife/pages/HomeContactUs.vue') },
  // === 卡包列表 ===
  { path: '/list', component: () => import('src/appWuhulife/pages/Index.vue') },
  { path: '/login', component: () => import('src/appWuhulife/pages/Login.vue') },
  { path: '/setting', component: () => import('src/appWuhulife/pages/Setting.vue') },
  { path: '/register', component: () => import('src/appWuhulife/pages/Register.vue') },
  { path: '/404' },
  // === 企業 APP 系列 ===
  // 企業首頁
  { path: '/card/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Index.vue') },
  // 會員頁
  { path: '/cardMember/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Member.vue') },
  // 積點紀錄
  { path: '/cardPoint/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Point.vue') },
  // 消費紀錄
  { path: '/cardPay', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Pay.vue') },
  // 優惠卷
  { path: '/cardCoupon/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Coupon.vue') },
  // 會員資訊
  { path: '/cardProfile', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Profile.vue') },
  // // 門市查詢
  { path: '/cardStore/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Store.vue') },
  // 產品列表頁
  { path: '/cardMenu/:id', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Menu.vue') },
  // // 產品介紹頁
  { path: '/cardProducts/:id', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Products.vue') },
  // 消息列表頁
  { path: '/cardNews/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/News.vue') },
  // 消息介紹頁
  { path: '/cardNewspage/:enterpriseId/:id', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Newspage.vue') },
  // // 常見問題頁
  { path: '/cardFaq', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/Faq.vue') },
  // 商品禮卷(寄杯)頁
  { path: '/cardGiftVouchers', meta: { layout: 'Card' }, component: () => import('src/appWuhulife/pages//cards/GiftVouchers.vue') },
]

export default new VueRouter({
  routes,
  scrollBehavior: function (to) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
  },
})
 
// fix相同的路由click出錯
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}