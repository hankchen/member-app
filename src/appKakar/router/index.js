// vue basic lib
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// vue router 設定(桌面版)
const routes = [
  // === 卡卡主頁面 ===
  { path: '/',      component: () => import('src/appKakar/pages/Index.vue') },
  { path: '/login', component: () => import('src/appKakar/pages/Login.vue') },
  { path: '/setting', component: () => import('src/appKakar/pages/Setting.vue') },
  { path: '/register', component: () => import('src/appKakar/pages/Register.vue') },
  { path: '/404' },
  // === 企業 APP 系列 ===
  // 企業首頁
  { path: '/card', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Index.vue') },
  // 會員頁
  { path: '/cardMember', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Member.vue') },
  // 積點紀錄
  { path: '/cardPoint', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Point.vue') },
  // 消費紀錄
  { path: '/cardPay', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Pay.vue') },
  // 優惠卷
  { path: '/cardCoupon', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Coupon.vue') },
  // 會員資訊
  { path: '/cardProfile', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Profile.vue') },
  // // 門市查詢
  { path: '/cardStore', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Store.vue') },
  // 產品列表頁
  { path: '/cardMenu/:id', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Menu.vue') },
  // // 產品介紹頁
  { path: '/cardProducts/:id', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Products.vue') },
  // 消息列表頁
  { path: '/cardNews', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/News.vue') },
  // 消息介紹頁
  { path: '/cardNewspage/:id', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Newspage.vue') },
  // // 常見問題頁
  { path: '/cardFaq', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/Faq.vue') },
  // 商品禮卷(寄杯)頁
  { path: '/cardGiftVouchers', meta: { layout: 'Card' }, component: () => import('src/appKakar/pages/cards/GiftVouchers.vue') },
]

export default new VueRouter({ routes })
