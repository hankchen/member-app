/* eslint-disable */
/* 與殼溝通相關的 function */

import axios from 'axios'
import store from 'src/store'
import router from 'src/appWuhulife/router'
import { showAlert } from 'src/libs/appHelper.js'

// 打 logger
const deviseLogger = function(act, params, cbFn) {
  // 時間戳記
  const getTimeStamp = function(t) {
    // 當下時間
    const time = t || new Date()
    // 補齊位數 => e.g. 1 -> 01 ,  51 -> 051
    const pad = function(num, maxLength) {
      // 要補齊的字數
      const diffLength = maxLength - num.toString().length
      return (new Array(diffLength + 1)).join('0') + num
    }
    // => sample: " @ 13:02:02.993"  (時:分:秒.毫秒)
    return  "@ " + (pad(time.getHours(), 2)) + ":" + (pad(time.getMinutes(), 2)) + ":" + (pad(time.getSeconds(), 2)) + "." + (pad(time.getMilliseconds(), 3));
  }

  console.groupCollapsed(`devise: ${act} ${getTimeStamp()}`);
  console.log('%c act',    'color: #afaeae; font-weight: bold', act);
  console.log('%c params', 'color: #9E9E9E; font-weight: bold', params);
  console.log('%c cbFn',   'color: #03A9F4; font-weight: bold', cbFn);
  console.groupEnd();
}

// 執行與殼溝通 function 的統一接口
const deviseFunction = function(act, params='', cbFn=''){
  if (!act) return console.log('deviseFunction: 請輸入 act')
  // 打 logger
  deviseLogger(act, params, cbFn)
  // 必須在殼裡面才能執行
  if (typeof(JSInterface) === 'undefined') return
  // 執行跟殼溝通的 function
  JSInterface.CallJsInterFace(act, params, cbFn)
}

// (藉由殼 API) 發送手機驗證簡訊
function sendDeviseSMSreCode(payload) {
  // payload sample: { phone: '0955111222;;886', SMSSendQuota: '20190514-3' }
  // 將已發送扣打紀錄存入殼裡
  deviseFunction('SetSP', `{"spName":"SMSSendQuota", "spValue":"${payload.SMSSendQuota}"}`, '')
  // 發送手機驗證簡訊
  deviseFunction('AppSMSReCode', payload.phone, '"cbFnSyncDeviseSMSreCode"')
}

// 與殼溝通，存取必要的資料
function ensureDeviseSynced() {
  // 判斷是否在殼裡面
  const isDeviseApp = store.state.isDeviseApp
  return new Promise(resolve => {

    // 不在殼裡 => 吃前端的預設參數, 直接繼續 fetchInitData
    if (!isDeviseApp) {
      //store.commit('setBaseInfo', {userTag:(VueCookies.get('userTag') || 'member')})//登入類型,member 會員 / employee 員工
      return resolve('ok')
    }

    // 清空舊的 setSP 資料
    deviseFunction('SetSP', `{"spName":"vipApps", "spValue":""}`, '')

    // 如果在殼裡
    //   1. 跟殼取 publicJWT, connect, isFrom, EnterpriseID, 會員登入資訊, 更新 store.state
    //   2. 跟殼取微管雲的系統參數: 每天最多簡訊次數, 每封簡訊發送間隔
    //   3. (以上都做完以後) => resolve('ok') // 繼續 fetchInitData
    deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
    deviseFunction('GetAppSysConfig', 'AppSMSIntervalMin,APPSMSDayTimes', '"cbFbSaveAppSysConfig"')
    resolve('ok')
  })
}

// 依據登入參數導向正確的網址
function setRedirectPage() {
  // 必須已登入
  if (!store.getters.isLogin) return

  // 判斷-正式/測試站
  const isWebProduction = /web/.test(location.host)
	,isWebStaging 		= /staging/.test(location.host)

  // 從 RtUrl 判斷 正式/ 測試站 帳號
  const {RtUrl} = store.state.userData
  const isAccountProduction = (RtUrl === '')

  if (!isAccountProduction) {
    let setThing = `{"spName":"Homehtml", "spValue": "${ RtUrl }"}`
    console.log('deviseHelper.js ===> isStaging =>' + setThing)

    // 如果在龍海正式站 && 登入的為測試帳號，導回到測試站
    if (isWebProduction) {
      console.log('deviseHelper.js  ===> production -> staging')
      alert('您位於正式站，登入的帳號為測試用帳號，將導向測試站台')

      // 在殼裡面 => 用restart重開網頁
      if (typeof(JSInterface) !== 'undefined') {
        redirectUrl = RtUrl
        if (store.state.userData.RtAPIUrl) deviseFunction('SetSP', `{"spName":"RtAPIUrl", "spValue": "${ store.state.userData.RtAPIUrl }"}`, '')
        deviseFunction('SetSP', setThing, '"cbFnRestart"')
      // 在瀏覽器 => 導網址
      } else {
        location.href = RtUrl
      }
    } else {
      deviseFunction('SetSP', setThing, '')
    }
  } else {
    let toUrl = 'https://web.jh8.tw/wuhulife/index.html'
    let setThing = `{"spName":"Homehtml", "spValue": "${ toUrl }"}`
    console.log('deviseHelper.js ===> isProduction =>' + setThing)

    // 如果在龍海測試站 && 登入的為正式帳號，導回到正式站
    if (isWebStaging) {
      console.log('deviseHelper.js  ===> staging -> production')
      alert('您位於測試站，登入的帳號為正式帳號，將導向正式站台')

      // 在殼裡面 => 用restart重開網頁
      if (typeof(JSInterface) !== 'undefined') {
        redirectUrl = toUrl
        deviseFunction('SetSP', `{"spName":"RtAPIUrl", "spValue": ""}`, '')
        deviseFunction('SetSP', setThing, '"cbFnRestart"')
      // 在瀏覽器 => 導網址
      } else {
        location.href = toUrl
      }
    } else {
      deviseFunction('SetSP', setThing, '')
    }
  }
}

// callback function: 儲存(從殼過來的) 基本設定資料
const cbFnSyncDeviseInfoToStore = function(e) {
  // showAlert('cbFnSyncDeviseInfoToStore===> e.pwd =>' + e.pwd)
  console.log('cbFnSyncDeviseInfoToStore ===> e=>', e)

  // 過濾，只取需要的資訊
  let baseInfoData = {}
  baseInfoData.pushNotify   = e.pushNotify // 是否推播訊息
  baseInfoData.WebVer       = e.WebVer     // 前端打包檔版本
  baseInfoData.brightness   = e.brightness // 螢幕亮度值
  baseInfoData.AppOS        = e.AppOS      // 殼的裝置 (iOS / Android)
  baseInfoData.currentEnterPriseID = e.currentEnterPriseID // 導向企業號
  baseInfoData.userTag = e.userTag || 'member'; //目前登入類型 member 會員 / employee 員工
  // 將資訊存入 state.baseInfo
  store.commit('setBaseInfo', baseInfoData)

  // 存入點擊紀錄
  if ( typeof(e.appClicks) != 'undefined' && e.appClicks != '' ) {
   const clicks = typeof(e.appClicks) === 'object' ? e.appClicks : JSON.parse(e.appClicks)
   store.commit('saveAppClicks', clicks)
  }

  // 有已登入的資訊 => 將會員資訊存入 store
  if ( typeof(e.Account) != 'undefined' && e.Account != '' ) {
    let userData = {}
    userData.mac          = e.mac
    userData.jwt          = e.jwt
    userData.Account      = e.Account
    userData.Name         = e.Name
    userData.Email        = e.Email
    userData.Address      = e.Address
    userData.CardNO       = e.CardNO
    userData.Birthday     = e.Birthday
    userData.Sex          = e.Sex
    userData.CardID       = e.CardID
    userData.CardTypeCode = e.CardTypeCode
    userData.CardTypeName = e.CardTypeName
    userData.ExpiredDate  = e.ExpiredDate
    userData.EnterPriseID = e.EnterPriseID
    userData.RtUrl        = e.RtUrl
    //console.log("tt>>",e.RtAPIUrl,store.state.userData.RtAPIUrl)
    if (e.RtAPIUrl) userData.RtAPIUrl = e.RtAPIUrl //防止登入後又被殼的蓋掉(若無值).
    if (store.state.userData.RtAPIUrl) userData.RtAPIUrl = store.state.userData.RtAPIUrl  //登入時,則用API回應的
    userData.pwd          = e.pwd
    userData.Tokenkey     = e.Tokenkey

    // 存入會員資訊
    store.commit('setLoginInfo', {data: userData, rememberMe: true, pwd: e.pwd})
    // 取會員資料
    store.dispatch('fetchVipApps')
    console.log('cbFnSyncDeviseInfoToStore ===> Tokenkey =>' + store.state.member.Tokenkey)
  }
}

// (藉由殼 API) 存入登入資訊
function deviseSetLoginInfo(payload) {
  console.log('deviseHelper ===> deviseSetLoginInfo')
  // payload sample: {type: 'set', data: res.data, rememberMe: true/false} , {type: 'reset'}

  // 預設值
  let data = {
    "Name": "",
    "Email": "",
    "Address": "",
    "CardNO": "",
    "Birthday": "",
    "Sex": 1,
    "mac": "",
    "CardID": "",
    "CardTypeCode": "",
    "CardTypeName": "",
    "ExpiredDate": "",
    "MembersName": "",
    "Account": "",
    "pwd": "",
    "note": "",
    "jwt": "",
    "RtUrl": "",
    "RtAPIUrl": "",
    "Tokenkey": ""
  }

  // 存入登入資料
  if (payload.type === 'set' && payload.rememberMe === true) {
    data = payload.data
    data['MembersName'] = payload.data.Name
    data['pwd'] = payload.pwd
    // 存入殼的推播設定
    deviseFunction('Do_Register', `{"MembersName":"${data.Name}", "Account":"${data.CardNO}"}`, '')
  }
  // 寫入要存的 key 設定
  data['_KEY'] = "MembersName,Name,Email,Address,CardNO,Birthday,Sex,mac,CardID,CardTypeCode,CardTypeName,ExpiredDate,Account,note,pwd,RtUrl,RtAPIUrl,Tokenkey"

  // 執行存入殼裡
  deviseFunction('SetSPS', JSON.stringify(data), '')

  // only for test logout
  deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
}

// callback function: 儲存(從殼過來的) 殼版本差異參數
const cbFnSetDeviseVersionIsDiff = function(e) {
  store.commit('SetDeviseVersionIsDiff', e.Obj)
}

let redirectUrl = ''
const cbFnRestart = function(e) {
  console.log('cbFnRestart ===> home')
  deviseFunction('home', '', '')
  // console.log('cbFnRestart ===> redirectUrl =>' + redirectUrl)
  // alert('cbFnRestart ===> redirectUrl =>' + redirectUrl)
  // location.href = redirectUrl
}

// callback function: 儲存(從殼過來的) 簡訊驗證碼
const cbFnSyncDeviseSMSreCode = function(e) {
  // 將簡訊驗證碼存入 state.SMS_Config
  store.commit('setSMSreCode', e)
}

// callback function: 儲存(從殼過來的) GPS 資料
const cbFnSyncDeviseGPS = function(e) {
  // console.log('cbFnSyncDeviseGPS ===> e =>', e)	// @@
  // 轉成 google maps api 需要的預設格式
  const gpsData = {gps: {lat: e.Lat, lng: e.Lng} }
  // 將 gps 存入 state.baseInfo
  store.commit('setBaseInfo', gpsData)
}

// callback function: 儲存微管雲系統參數
const cbFbSaveAppSysConfig = function(e) {
  // 目前只有取 APPSMSDayTimes(每天最多簡訊次數), AppSMSIntervalMin(每封簡訊發送間隔) 這二個值，只需要存這二個值就好
  const data = {
    APPSMSDayTimes: e.APPSMSDayTimes,
    AppSMSIntervalMin: e.AppSMSIntervalMin
  }
  store.commit('setSMS_Config', data)
}

// callback function: 存入每天簡訊發送的扣打紀錄
const cbFnSetSMSSendQuota = function(e) {
  const data = { SMSSendQuota: e }
  store.commit('setSMS_Config', data)
}

// callback function: 存入字體大小
const cbFnSetFontSize = function(e) {
  // console.log('===> e =>' + e)
  store.commit('setFontSizeTemp', e)
}

// callback function: 掃描卡卡綁定企業 app 的 QRcode
const cbFnScanKakarAppsQrCode = function(QrCode) {
  // QrCode =  QrCode.replace('http://www.jinher.com.tw/Qrcode/QrcodeEntry.html?QrCode=','')
  // QrCode = atob(QrCode);
  // QrCode = QrCode.split("&EnterpriseID=")[0];
  QrCode =  QrCode.replace('http://www.jinher.com.tw/Q/','')
  QrCode =  QrCode.replace('.html','')

  if (typeof(QrCode) !== 'string') return showAlert('條碼資料錯誤！')

  const requestBody = Object.assign({}, store.getters.kakarBody, {QrCode: QrCode}, { "act": "SetKKVipAPP" })
  const requestHead = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
  axios
    .post('https://wuhuapp.jh8.tw/Public/AppVipOneEntry.ashx', requestBody, requestHead)
    .then(res => {
      showAlert(res.data.ErrorMsg)

      if (res.data.ErrorCode === '0') {
        store.commit('setInputQRCode', false)
        store.dispatch('fetchVipApps')
      }
    })
}
// callback function: 掃瞄QRcode wufu辦活動用
const cbFnScanWuhuQrCode = function(QrCode) {
  //if (typeof(QrCode) !== 'string') return showAlert('條碼資料錯誤！')
  store.commit('initQrCodeItem');//初始化由掃瞄生成的品項
  if (store.getters.isLogin) {
    QrCode = QrCode.replace('＆','&');
    const isFullUrl = /http:|https:/.test(QrCode)
    const isOkUrl = /surl.jh8.tw/.test(QrCode) && isFullUrl
    const isScanResult = (QrCode === "shop?type=scanResult");
    const isScanSpecShop = (QrCode.indexOf("shop?type=specShop&list=") == 0);
    if (isScanSpecShop) deviseFunction('setIO', '1', '')
    if (!isOkUrl && !isScanResult && !isScanSpecShop) showAlert('無效的掃瞄碼')
    if (isScanResult || isScanSpecShop) router.replace((QrCode.indexOf('/') == 0?QrCode:'/'+QrCode));  //沒有'/'時,會在其Layout下開,在卡包內會抓不到barcode功能

    if (isOkUrl){

      store.dispatch('fetchQrcodUrl',{url:QrCode})
      //deviseFunction('openWeburl', QrCode, '')
    }


  }else{
    router.replace({path: '/'})
  }
}
const cbFnScanQrCode = function(eObj) {
  console.log('cbFnScanQrCode ===> e.Obj =>', eObj)

  // 必須已登入
  if (!store.getters.isLogin) {
    console.log('===> 未登入 => 到首頁')
    router.push({path: '/login'})
  } else {
    let path = ''
    if (eObj) {
      path = eObj.path

      if (path != '') {
        path = '/' + path
      }
    }

    console.log('===> push to =>' + path)
    router.push({path: path})
  }
}


// -- 使用 jsInterFace 跟殼取資料的用法(目前有用到的) --


// GetSPAll => 取全部存入殼的資訊
// JSInterface.CallJsInterFace('GetSPAll', '', '')

// // SetSPS            => 設定多值
// data = { "_KEY": "InvType,InvDes,InvVend", "InvType": "InvType", "InvDes": "InvDes", "InvVend": "InvVend" }
// JSON.stringify(data) // '{"_KEY":"InvType,InvDes,InvVend","InvType":"InvType","InvDes":"InvDes","InvVend":"InvVend"}'
// JSInterface.CallJsInterFace('SetSPS', JSON.stringify(data), '');
// // SetSP             => 將資料存入殼裡
// JSInterface.CallJsInterFace('SetSP', '{"spName":"SMSCode", "spValue":"2345222"}', '')
// // GetSP             => 跟殼取資料
// JSInterface.CallJsInterFace('GetSP', 'SMSCode', '')
// // gps               => 取得 GPS 地理資訊
// JSInterface.CallJsInterFace('gps', '', '')
// // GetAppSysConfig   => 取得殼的系統參數
// JSInterface.CallJsInterFace('GetAppSysConfig', 'AppRegNeedSMSCheck,AppSMSIntervalMin,APPSMSDayTimes', '')
// // getAppPublicToken => 取公用接口 token
// JSInterface.CallJsInterFace('getAppPublicToken', '', '')
// // AppSMSForgetPW    => 會改會員的帳號密碼，並將新的密碼簡訊發送給會員
// JSInterface.CallJsInterFace('AppSMSForgetPW', '0956241782', '')
// // AppSMSReCode      => 發送驗證簡訊 => callback 會回傳驗證碼 => 也可以用 '0956241782;hello' 自訂驗證碼
// JSInterface.CallJsInterFace('AppSMSReCode', '0956241782', '' );
// // getBrightness     => 取得目前螢幕亮度
// JSInterface.CallJsInterFace('getBrightness', '', '');
// // setBrightness     => 調整螢幕亮度(最亮值 255)
// JSInterface.CallJsInterFace('setBrightness', '255', '');
// // barcode           => 開啟掃描器
// JSInterface.CallJsInterFace('barcode', '', '');
// // openWeburl        => 開啟外部連結
// JSInterface.CallJsInterFace("openWeburl", link, '')
// // tel               => 啟用撥打電話功能
// JSInterface.CallJsInterFace("tel", '0988123123', '')
// // shareTo           => 啟用分享功能
// JSInterface.CallJsInterFace('shareTo', '{"subject":"給目標的抬頭", "body":"給目標的內容", "chooserTitle":"開啟分享時的抬頭"}', '');
// // setIO             => 離開 -> 再進入畫面時，不用再重新 loading
// JSInterface.CallJsInterFace('setIO', '1', '')
// // onFrontEndInited  => 通知殼：前端已經初始化完畢 (殼才會執行 returnJsInterFace )
// JSInterface.CallJsInterFace('onFrontEndInited', '', '')
// // mailto            => 啟用殼的寄信功能
// JSInterface.CallJsInterFace('mailto', '{"mailsubject":"給目標的抬頭", "mailbody":"給目標的內容", "chooserTitle":"開啟分享時的抬頭", "mailreceiver":"sdlong.jeng@gmail.com"}', '')
// // Do_Register       => 推播設定, 存入 App 推播所需要的參數
// JSInterface.CallJsInterFace('Do_Register', '{"MembersName":"Allen", "Account":"0956241782"}', '')
// // clearchache       => 清除網頁 cache
// JSInterface.CallJsInterFace('clearchache', '', '')

/* callback function 集中於此 */

// -- APP 殼的公用 callback 接口 (所有打向殼的 API，都會由殼裡來 call 此 function，回傳資料) --
// 解說： 跟殼溝通的接口

// sample: JSInterface.CallJsInterFace('GetString', 'Account,mac', console.log('hello'))
// => data: {State: "OK", Type: "GetString", Obj: "177f8e5494d251e62ffd68c9dfe903a2"}
// => 殼會執行 window.returnJsInterFace(data, console.log('hello'))
// 所以此 function 是用來接收殼回傳的資訊，並做對應的邏輯處理

// !!重要!! => 由於 returnJsInterFace 被 public 出去，所以任何人都能在 console 執行 returnJsInterFace 來亂 try
// !!重要!! => 所以在裡面的 callback function 務必設定成白名單形式，只接受合法的，不然會造成資安漏洞
const returnJsInterFace = (data, cbFn) => {
  console.log('returnJsInterFace receive:', data, ", cbFn:", cbFn)

  try {
    // data sample:
    // {State: "OK", Type: "gps", Obj: "{Lat: 37.785835, Lng: -122.406418}"}
    // {State: "OK", Type: "GetAppSysConfig", Obj: {AppRegNeedSMSCheck: "false", AppSMSIntervalMin: "10", APPSMSDayTimes: "3"}}

    // 依據 Type 不同執行各自的邏輯，這樣設計的用意是做白名單，防止被亂 try
    // Policy:
    //   1. !!重要!! 取得資料後要執行的邏輯一律做在 callback function (e.g. 拿取得的資料存入 store, 打 api , 做 xxx 事情)
    //   2. !!重要!! 這裏只做從殼回傳資料的『驗證與轉換』
    //   3. 在 iOS 的 cbFn 是 return fuction, 在 Android 是 return string
    switch(data.Type) {
      // 將多筆資料存入殼裡 (不做 callback)
      case 'SetSPS': break;
      // 設定離開 app 再進入時是否要 reload app (不做 callback)
      case 'setIO': break;
      // 殼的螢幕亮度 (不做 callback)
      case 'setBrightness': break;
      // 將資料存入殼裡 (不做 callback)
      case 'SetSP':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnRestart') cbFnRestart(data.Obj)
      break;
      // 將資料(多筆)從殼取出 (因本專案沒用到 GetSPS, 所以不做 callback)
      case 'GetSPS': break;
      // 啟用分享功能 (不做 callback)
      case 'shareTo': break;
      // 取公用接口 jwt (通常只有第一次安裝才會用到) (不做 callback)
      case 'getAppPublicToken': break;
      // 推播設定, 存入 App 推播所需要的參數 (不做 callback)
      case 'Do_Register': break;

      // 將資料從殼取出
      case 'GetSP':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSetSMSSendQuota') cbFnSetSMSSendQuota(data.Obj)
        if (cbFn === 'cbFnSetFontSize') cbFnSetFontSize(data.Obj)
      break;

      // 將全部存入殼的資料取出
      case 'GetSPAll':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseInfoToStore') cbFnSyncDeviseInfoToStore(data.Obj)
      break;

      // 取系統參數 => 目前只有取 APPSMSDayTimes(每天最多簡訊次數), AppSMSIntervalMin(每封簡訊發送間隔) 這二個值
      case 'GetAppSysConfig':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFbSaveAppSysConfig') cbFbSaveAppSysConfig(data.Obj)
      break

      // 取 gps 資訊
      case 'gps':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseGPS') cbFnSyncDeviseGPS(data.Obj)
      break;

      // 發送簡訊驗證碼
      case 'AppSMSReCode':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseSMSreCode') cbFnSyncDeviseSMSreCode(data.Obj)
      break;

      // email 寄信功能
      case 'mailto':
        // (只有在 iOS 才有此狀況) 電子郵件未設定，跳出請去設定的訊息
        if (data.State === 'error' && data.Obj === '手機電子郵件未設定') showAlert('手機電子郵件未設定, 請參考 <a href="https://support.apple.com/zh-tw/HT201320">電子郵件設定</a>')
      break;

      // 掃條碼
      case 'barcode':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (data.State !== 'error' && cbFn === 'cbFnScanKakarAppsQrCode') cbFnScanKakarAppsQrCode(data.Obj)
        else if (data.State !== 'error' && cbFn === 'cbFnScanWuhuQrCode') cbFnScanWuhuQrCode(data.Obj)
      break;

      // 存入殼是否需要更新的參數
      case 'versionIsDiff':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (data.State !== 'error' && cbFn === 'cbFnSetDeviseVersionIsDiff') cbFnSetDeviseVersionIsDiff(data)
      break;
      case 'applicationDidEnterBackground':
        if (store.getters.isLogin) deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
      break;  
      // 離開 app 又回去時，回到卡卡首頁
      case 'applicationDidBecomeActive':
        if (router.app.$route.path == '/shopCart' && router.app.$route.query.type == 'finishedTemp') store.commit('setBecomeApp') //商城刷新檢查
        if (router.app.$route.path == '/shopMine' && router.app.$route.query.type == 'finishedTemp') store.commit('setBecomeApp') //商城記錄刷新檢查        
        if (store.getters.isLogin) deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
        if (store.state.appSite !== 'kakar') return

        store.commit('setLoading', true)
        setTimeout(() => { store.commit('setLoading', false) }, 400)
        store.commit('clearAppPublicData')
        store.commit('clearAppMemberData')
        router.push('/')
        // 重新讀取會員卡列表
        store.dispatch('fetchVipApps')
      break;
      // 離開 app 又回去時，回到卡卡首頁
      case 'wuhulifeapp':
        console.log('wuhulifeapp ===> data =>', data.Obj)
        // alert('wuhulifeapp ===>' + JSON.stringify(data.Obj))
        cbFnScanQrCode(data.Obj)
      break;
      // 其他未設定到的，打 console.warn 來做提醒
      default:
        console.warn('type not defined :', data.Type)

    }
  } catch(error) {
    // 若有任何例外錯誤，要跳 console.error 以利查 bug
    console.error('returnJsInterFace Exception Error: ', error);
  }
}

export { deviseFunction, returnJsInterFace, ensureDeviseSynced, deviseSetLoginInfo, sendDeviseSMSreCode, setRedirectPage }
