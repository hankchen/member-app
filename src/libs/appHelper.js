/* app 通用 helper  */

import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
Vue.use(VueCompositionApi)

import store from '../store'
import { computed } from '@vue/composition-api'

// toast
import { Toaster } from "@blueprintjs/core"
const AppToaster = Toaster.create({ position: 'top', maxToasts: 2 })

// 觸發 Toast (訊息通知元件)
function showToast(message, config = {}) {
  const baseConfig = {intent: 'primary', icon: 'notifications', timeout: 2500 }
  const data = Object.assign({}, baseConfig, config, {message: message})
  return AppToaster.show(data)
  /**
    # 用法：

    * 一般
    => app.showToast('hello world')

    * 加入客制色系 & icon 的用法 (預設: {intent: 'primary', icon: 'notifications' } )
    => app.showToast('hello world', {intent: 'success', icon: 'issue' })

    * 可用參數：
    intent : primary, success, danger, warning
    icon   : 參考 https://blueprintjs.com/docs/#icons
  **/
}

function setLoading(status) { store.commit('setLoading', status) }

// 顯示自訂的 alert 畫面
function showAlert(text) { store.commit('setCustomModal', { type: 'alert', text }) }
function hideModal() { store.commit('setCustomModal', { type: '', text: '' }) }

// 顯示自訂的 confirm 畫面
// text: 大標題
// confirmText: 確認按鈕label
// cancelText: 取消按鈕label
// confirmNoteTitle: 附註標題
// confirmNoteChoice: 附註選項
// confirmNoteChoosed: 附註選項已選
// confirmNoteRemark: 附註選了其他要手key的說明
// confirmNoteRemarkPlaceHolder: 其他原因的place holder
// confirmNoteWarnMsg: 警告訊息
function showConfirm(text, confirmText, cancelText,
  confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
  confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg) {
  return new Promise((resolve, reject) => {
    store.commit("setCustomModal", {
      type: 'confirm',
      text, confirmText, cancelText, 
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg,
      resolve, reject
    });
  });
}

// 依據 cssTouch 參數設置 mobile 滑動的 css 參數
const touchScrolling = computed(() => {
  const cssTouch = store.state.cssTouch
  return {'-webkit-overflow-scrolling': (cssTouch) ? 'touch' : 'auto' }
})

// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImage(imageUrl) {
  let isImage = false
  if (imageUrl) {
    // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
    isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl.toLowerCase())
  }
  // 若不是圖檔，回傳 noImageUrl
  if (isImage === false) return 'https://web.jh8.tw/wuhulife/img/img_init.gif'

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  return store.getters.srcUrl + imageUrl
}

// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImageWuhu(imageUrl) {
  let isImage = false
  if (imageUrl) {
    // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
    isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl.toLowerCase())  
  }
  
  // 若不是圖檔，回傳 notImage 字串
  if (isImage === false) return 'https://web.jh8.tw/wuhulife/img/img_init.gif'

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  return store.state.api.picUrl + imageUrl
}

// 判斷是否已登入
const isLogin = computed(() => store.getters.isLogin )

export { showToast, showAlert, showConfirm, setLoading, hideModal, touchScrolling, isLogin, showImage, showImageWuhu, }
